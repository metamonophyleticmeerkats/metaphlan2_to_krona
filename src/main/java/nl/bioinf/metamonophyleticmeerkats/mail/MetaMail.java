package nl.bioinf.metamonophyleticmeerkats.mail;

import nl.bioinf.metamonophyleticmeerkats.globalutilities.ConfigProperties;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MetaMail implements FormattedTimeInterface, ConfigProperties {
    /**
     * Create a session object with login. (The session does not needs to be closed.)
     */
    public Session metaMailLogin() {
        // Mail server specifications.
        Properties props = new Properties();
        // smtpServerHost.
        props.setProperty("mail.smtp.host", metaToKronaMailConfigProperties.get("smtpHost"));
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", "465");
        // Create user login.
        return Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(metaToKronaMailConfigProperties.get("username"), metaToKronaMailConfigProperties.get("password"));
                    }
                });
    }
    /**
     * Method that sends an email by receiving a login session, an address a subject and a message.
     */
    public void metaMailSendMail(Session currentSession, String mailAddress, String subject, String inputMessage){
        try {
            // Create a new message object.
            Message message = new MimeMessage(currentSession);

            // Get senders email address.
            message.setFrom(new InternetAddress(metaToKronaMailConfigProperties.get("internetAddress")));

            // Setup a message for the receiver(s) email address.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mailAddress));

            // Set the message for the subject.
            message.setSubject(subject);

            // Make the message itself from a String.
            message.setText(inputMessage);

            // Send the mail to the receiver(s).
            Transport.send(message);

        } catch (MessagingException e) {
            // Print out the error message with stacktrace for the server if it was unable to sent mail.
            System.out.println(formattedTime+"Something went wrong while try to send mail!");
            System.out.println(formattedTime+e);
            e.printStackTrace();
        }


    }
    /**
     * Removes outgoing mail in gmail, so no information of users gets saved on the mail address
     */
    public void metaMailRemoveOutgoingMail(Session currentSession) {
        try {
            // Accessing mail box.
            Store store = currentSession.getStore("imaps");
            store.connect(metaToKronaMailConfigProperties.get("smtpHost"), metaToKronaMailConfigProperties.get("internetAddress"), metaToKronaMailConfigProperties.get("password"));


            // Go the the sent mail directory. !!! This currently only works for GMAIL!!!!
            Folder sentMailFolder = store.getFolder("[Gmail]/Sent Mail");
            sentMailFolder.open(Folder.READ_WRITE);
            // Message array of all sent mails.
            Message[] sentMessages = sentMailFolder.getMessages();

            // Delete all messages in the sent mail box.
            for (int i = 0; i < sentMailFolder.getMessageCount(); i++){
                Message message = sentMessages[i];
                message.setFlag(Flags.Flag.DELETED, true);
            }
        } catch ( MessagingException e) {
            // Print out the error message with stacktrace for the server if it was unable to remove all sent mail.
            System.out.println(formattedTime+"Something went wrong when trying to delete outgoing mail!");
            System.out.println(formattedTime+e);
            e.printStackTrace();
        }


    }
}
