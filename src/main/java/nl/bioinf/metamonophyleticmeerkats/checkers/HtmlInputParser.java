package nl.bioinf.metamonophyleticmeerkats.checkers;

import nl.bioinf.metamonophyleticmeerkats.database.DatabaseEntry;
import nl.bioinf.metamonophyleticmeerkats.database.DatabaseManagement;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.HashGenerator;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses html form from upload page.
 */
public class HtmlInputParser implements FormattedTimeInterface {

    // Type of search used by Bowtie2 in MetaPhlAn 2.
    private final String[] BOWTIEPRESETS = {"sensitive", "very-sensitive", "sensitive-local", "very-sensitive-local"};
    // Selections of levels which can be used to specify a range of organisms (all taxonomic levels to species level).
    private final String[] TAXONOMICLEVELS = {"a", "k", "p", "c", "o", "f", "g", "s"};
    // EXPERIMENTAL !!! Statistical aproaches for converting marker abundances into clade abundances.
    private final String[] STATISTICALAPPROACHES = {"avg_g", "avg_l", "tavg_g", "tavg_l", "wavg_g", "wavg_l", "med"};
    // Types of MetaPhlAn 2 analyses.
    private final String[] ANALYSISTYPES = {"rel_ab", "rel_ab_w_read_stats", "reads_map", "clade_profiles",
            "marker_ab_table", "marker_pres_table"};

    // Variables that are inserted into the database.
    private HttpServletRequest rawRequest;
    private ArrayList<String> fileInputs;
    private String markerFileInput;
    private String submissionDate;
    private String expirationTime;
    private String jobName;
    private String emails;
    private String btPs;
    private String taxLev;
    private String minCuLenRaw;
    private String minCuLen;
    private String minAlignmentLenRaw;
    private String minAlignmentLen;
    private String ignoreVirussesRaw;
    private boolean ignoreVirusses;
    private String ignoreEukaryotesRaw;
    private boolean ignoreEukaryotes;
    private String ignoreBacteriaRaw;
    private boolean ignoreBacteria;
    private String ignoreArchaeaRaw;
    private boolean ignoreArchaea;
    private String statQRaw;
    private String statQ;
    private String avoidDisQMRaw;
    private boolean avoidDisQM;
    private String stat;
    private String analysisType;
    private String nReadsRaw;
    private String nReads;
    private String presTHRaw;
    private String presTH;
    private String clade;
    private String minABRaw;
    private String minAB;
    private int jobHashSize;
    private String hash;

    // Checks input for all variables of the metaToKrona input page.
    public HtmlInputParser(String hash, HttpServletRequest request, ArrayList<String> fileInputs, String markerFileInput,
                           long submissionDate, long expirationTime, int jobHashSize) {

        this.rawRequest = request;
        this.fileInputs = fileInputs;
        this.markerFileInput = markerFileInput;
        this.submissionDate = Long.toString(submissionDate);
        this.expirationTime = Long.toString(expirationTime);
        this.jobName = this.rawRequest.getParameter("jobName");
        this.emails = this.rawRequest.getParameter("emailAddress");
        this.btPs = this.rawRequest.getParameter("bt2_ps");
        this.taxLev = this.rawRequest.getParameter("tax_lev");
        this.minCuLenRaw = this.rawRequest.getParameter("min_cu_len");
        this.minAlignmentLenRaw = this.rawRequest.getParameter("min_alignment_len");
        this.ignoreVirussesRaw = this.rawRequest.getParameter("ignore_virusses");
        this.ignoreArchaeaRaw = this.rawRequest.getParameter("ignore_archaea");
        this.ignoreEukaryotesRaw = this.rawRequest.getParameter("ignore_eukaryotes");
        this.ignoreBacteriaRaw = this.rawRequest.getParameter("ignore_bacteria");
        this.statQRaw = this.rawRequest.getParameter("stat_q");
        this.avoidDisQMRaw = this.rawRequest.getParameter("avoid_disqm");
        this.stat = this.rawRequest.getParameter("stat");
        this.analysisType = this.rawRequest.getParameter("analysis_type");
        this.nReadsRaw = this.rawRequest.getParameter("nreads");
        this.presTHRaw = this.rawRequest.getParameter("pres_th");
        this.clade = this.rawRequest.getParameter("clade");
        this.minABRaw = this.rawRequest.getParameter("min_ab");
        this.jobHashSize = jobHashSize;
        this.hash = hash;
    }
    /**
     * Receives all the inputs from the webpage and validates if these inputs are correct.
     * If the inputs are correct they are converted to the correct data types for the database.
     */
    public HttpServletRequest checkAllInputsAndSubmit(String databaseLocation) {
        // Starts with true if input is invalid it will it turns to false and the job will not be submitted.
        boolean areInputsCorrect = true;

        // Validates if it has one or multiple numbers followed with a . character and is
        // followed by one or multiple numbers.
        String intAndDoublePattern = "(\\d+\\.\\d+)|\\d+";
        // Checks the file extension of the input files first.
        // Instantiate a new fileExtensionChecker object to validate the input files and the marker file.
        FileExtensionChecker fileExtensionChecker = new FileExtensionChecker(this.fileInputs);
        boolean filesAreCorrect = fileExtensionChecker.checkFiles();
        if (this.fileInputs.isEmpty()) {
            filesAreCorrect = false;
        }
        String fileFormat = fileExtensionChecker.getGlobalFileFormat();
        // Checks the file extension of the marker file.
        // Marker files are not mandatory for running the pipeline, they have their own boolean value.
        boolean markerFileIsCorrect = true;
        if (!(this.markerFileInput == null)) {
            String[] splitString = this.markerFileInput.split("\\.");
            // Acquire extension of marker file.
            String fileExtension = splitString[splitString.length - 1];
            // Check extensions manually because FileExtensionChecker is specific for input files but not for the
            // marker.
            if (!fileExtension.equals("fasta") && !fileExtension.equals("fa")) {
                markerFileIsCorrect = false;
            }
        }
        else {
            // It seems there is no marker file.
            this.markerFileInput = null;
        }
        if (!(filesAreCorrect && markerFileIsCorrect)) {
            this.rawRequest.setAttribute("faultyFiles", "Either%20your%20input%20files%20or%20marker%20file%20is%20in%20correct%2C%20please%20check");
            areInputsCorrect = false;
        }
        // check all normal strings
        // check jobname, if not defined, make a jobname.
        if (this.jobName.length() > 0) {
            if (isGivenInputAnInjection(this.jobName) || this.jobName.length() > 255) {
                this.rawRequest.setAttribute("faultyJobname", "your%20jobname%20either%20contains%20illegal%20characters%20(%5C%22%3B%7B%7D()%5B%5D%27%3F%25%3D*%20)%20or%20is%20too%20long%20(250%3Dmax%20length)");
                areInputsCorrect = false;
            }
        }
        else {
            HashGenerator jobHash = new HashGenerator();
            jobName = jobHash.generateHash(this.jobHashSize);
            jobName = "job_"+ jobName;
        }
        // check emails
        if (this.emails.length() <= 0 || isGivenInputAnInjection(this.emails) || !this.emails.matches("(([^,@]+@[^,@]+\\.[^,@]+)(,([^,@]+@[^,@]+\\.[^,@]+))*)")) {
            this.rawRequest.setAttribute("faultyEmails", "Email(s)%20is%2Fare%20wrongly%20specified%20and%2For%20contains%20illegal%20characters%20(%5C%22%3B%7B%7D()%5B%5D%27%3F%25%3D*%20)");
            areInputsCorrect = false;
        }
        // check clade option
        if (isGivenInputAnInjection(this.clade)) {
            this.rawRequest.setAttribute("faultyClade", "clade%20is%20wrongly%20specified%20and%2For%20contains%20illegal%20characters%20(%5C%22%3B%7B%7D()%5B%5D%27%3F%25%3D*%20)");
            areInputsCorrect = false;
        }
        else if (this.clade.isEmpty()) {
            this.clade = null;
        }
        // check all radio options
        // check bt2_preset
        if (!isPresentInArray(this.btPs, this.BOWTIEPRESETS)) {
            this.rawRequest.setAttribute("faultyBtps", "Bowtie%20preset%20is%20somehow%20faulty%2C%20did%20you%20try%20to%20omit%20the%20default%20form%3F");
            areInputsCorrect = false;
        }
        // check tax level
        if (!isPresentInArray(this.taxLev, this.TAXONOMICLEVELS)) {
            this.rawRequest.setAttribute("faultyTaxLev", "taxonomic%20level%20is%20somehow%20faulty%2C%20did%20you%20try%20to%20omit%20the%20default%20form%3F");
            areInputsCorrect = false;
        }
        // check statisical approach
        if (!isPresentInArray(this.stat, this.STATISTICALAPPROACHES)) {
            this.rawRequest.setAttribute("faultyStat", "stat%20is%20somehow%20faulty%2C%20did%20you%20try%20to%20omit%20the%20default%20form%3F");
            areInputsCorrect = false;
        }
        // check analysis type
        if (!isPresentInArray(this.analysisType, this.ANALYSISTYPES)) {
            this.rawRequest.setAttribute("faultyAnalysisType", "analysis%20type%20is%20somehow%20faulty%2C%20did%20you%20try%20to%20omit%20the%20default%20form%3F");
            areInputsCorrect = false;
        }
        // check all integers
        // check pres_th
        if (this.presTHRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.presTH = parseInteger(this.presTHRaw);
        }
        else if (this.presTHRaw.isEmpty()) {
            this.presTH = null;
        }
        else {
            this.rawRequest.setAttribute("faultyPresTH", "pres_th%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        // check min cu length
        if (this.minCuLenRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.minCuLen = parseInteger(this.minCuLenRaw);
        }
        else if (this.minCuLenRaw.isEmpty()) {
            this.minCuLen = null;
        }
        else {
            this.rawRequest.setAttribute("faultyMinCuLen", "min_cu_len%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        // check min alignment length
        if (this.minAlignmentLenRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.minAlignmentLen = parseInteger(this.minAlignmentLenRaw);
        }
        else if (this.minAlignmentLenRaw.isEmpty()) {
            this.minAlignmentLen = null;
        }
        else {
            this.rawRequest.setAttribute("faultyMinAlignmentLen", "min_alignment_len%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        // check nreads
        if (this.nReadsRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.nReads = parseInteger(this.nReadsRaw);
        }
        else if (this.nReadsRaw.isEmpty()) {
            this.nReads = null;
        }
        else {
            this.rawRequest.setAttribute("faultyminNreads", "nreads%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        // define all checkboxes as correct booleans
        this.ignoreVirusses = parseCheckbox(this.ignoreVirussesRaw);
        this.ignoreBacteria = parseCheckbox(this.ignoreBacteriaRaw);
        this.ignoreArchaea = parseCheckbox(this.ignoreArchaeaRaw);
        this.ignoreEukaryotes = parseCheckbox(this.ignoreEukaryotesRaw);
        this.avoidDisQM = parseCheckbox(this.avoidDisQMRaw);
        // check all floats / doubles
        // check statq
        if (this.statQRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.statQ = parseDouble(this.statQRaw);
        }
        else if (this.statQRaw.isEmpty()) {
            this.statQ = null;
        }
        else {
            this.rawRequest.setAttribute("faultyStatQ", "stat_q%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        // check min_ab
        if (this.minABRaw.matches(intAndDoublePattern)) {
            // define actual value now.
            this.minAB = parseDouble(this.minABRaw);
        }
        else if (this.minABRaw.isEmpty()) {
            this.minAB = null;
        }
        else {
            this.rawRequest.setAttribute("faultyMinAB", "min_ab%20is%20not%20a%20correct%20digit%20or%20float.");
            areInputsCorrect = false;
        }
        if (areInputsCorrect) {
            // submit to db
            System.out.println(formattedTime +"Submitting form to db!");
            DatabaseEntry checkedEntry = new DatabaseEntry(this.hash, this.fileInputs, this.markerFileInput,
                    this.submissionDate, this.expirationTime, this.jobName, this.emails, this.btPs, this.taxLev,
                    this.minCuLen, this.minAlignmentLen, this.ignoreArchaea, this.ignoreBacteria,
                    this.ignoreEukaryotes, this.ignoreVirusses, this.statQ, this.avoidDisQM, this.stat,
                    this.analysisType, this.nReads, this.presTH, this.clade, this.minAB, fileFormat);
            // parse object to database and insert the entry.
            try {
                DatabaseManagement database = new DatabaseManagement(databaseLocation);
                database.createNewQueueEntry(checkedEntry, this.jobName);
                database.closeSQLDBConnection();
                this.rawRequest.setAttribute("uploadstate", "Your%20job%20has%20been%20succesfully%20submitted!");
            }
            catch (SQLException se) {
                System.out.println(formattedTime +"Something went wrong connecting to the database or inserting a query");
                this.rawRequest.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%2C%20contact%20your%20server%20administrator%20if%20this%20persists.");
                System.out.println(formattedTime +se);
                se.printStackTrace();
            }
            catch (ClassNotFoundException cnfe) {
                System.out.println(formattedTime +"Something went wrong when loading sqlite library, please check your installation.");
                this.rawRequest.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%2C%20contact%20your%20server%20administrator%20if%20this%20persists.");
                System.out.println(formattedTime +cnfe);
                cnfe.printStackTrace();
            }
        }
        else {
            this.rawRequest.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%20because%201%20or%20more%20parameters%20were%20incorrect.");
        }
        return this.rawRequest;
    }
    /**
     * Checks the input of the fields to prevent sql injection,
     * it is quite strict so odd filenames and odd email names could cause issues.
     * Returns match that could potentially be a sql injection.
     */
    private boolean isGivenInputAnInjection(String myInput) {
        Pattern p = Pattern.compile("[\\\";\\(\\){}\\'?%\\[\\]=\\*\\t\\n\\r ]");
        Matcher m = p.matcher(myInput);
        return m.find();
    }

    /**
     * Validates if the String element is in the array, if not returns false else true.
     */
    private boolean isPresentInArray(String element, String[] elementArray) {
        for(String str: elementArray) {
            if(str.trim().contains(element))
                return true;
        }
        return false;
    }
    /**
     * Converts a String into an int if it consists of a number that is either a double or int
     * (found by the regex).
     */
    private String parseInteger(String rawInput) {
        if (rawInput.matches("\\d+")) {
            return Integer.toString(Integer.parseInt(rawInput));
        }
        else {
            // If input is double, cast it to an Integer to avoid problems.
            return Integer.toString((int) Double.parseDouble(rawInput));
        }
    }
    /**
     * Converts a String into a double if it consists of a number that is either a double or int
     * (found by the regex).
     */
    private String parseDouble(String rawInput) {
        if (rawInput.matches("\\d+")) {
            // If input is Integer, cast it to a double to avoid problems.
            return Double.toString((double) Integer.parseInt(rawInput));
        }
        else {
            return Double.toString(Double.parseDouble(rawInput));
        }
    }
    /**
     * Validates the input for the checkbox, it returns a false if it is not checked
     * or has other input then the string "on".
     */
    private boolean parseCheckbox(String rawInput) {
        return rawInput != null && !rawInput.isEmpty() && rawInput.equals("on");
    }
}
