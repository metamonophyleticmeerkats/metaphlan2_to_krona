package nl.bioinf.metamonophyleticmeerkats.checkers;

import java.util.ArrayList;

/**
 * Checks file extension of regular file inputs
 */
class FileExtensionChecker {

    private ArrayList<String> inputFiles = new ArrayList<>();
    private String globalFileFormat = "";

    /**
     * Declares files.
     */
    FileExtensionChecker(ArrayList<String> inputFiles) {
        // Declare files.
        this.inputFiles = inputFiles;
    }

    /**
     * Return global file format.
     */
    String getGlobalFileFormat() {
        return globalFileFormat;
    }

    /**
     * Returns a boolean telling if all the files are wrong or not.
     */
    boolean checkFiles() {
        return checkForMixedAndWrongExtensions();
    }

    /**
     * Get the file extension by splitting the name of the file on the . character and
     * acquire the last split product.
     */
    private String getExentsion(String stringy) {
        String[] splittedString = stringy.split("\\.");
        return splittedString[splittedString.length - 1];
    }
    /**
     * Checks if file type is correct, only fasta, fastq and sam files are allowed with their shortened extensions.
     * fastq, fq
     * fasta, fa
     * sam
     * Returns a boolean value.
     */
    private boolean checkForMixedAndWrongExtensions() {
        boolean goodExtensions = true;

        for (String fileInput : this.inputFiles) {
            // Check if the fileinput has the same extension as the global one. if not, extension combination is faulty.
            String fileInputExtension = this.getExentsion(fileInput);

            if (this.globalFileFormat.equals("")) {
                switch (fileInputExtension) {
                    case "fastq":
                    case "fq":
                        this.globalFileFormat = "fastq";
                        break;
                    case "fasta":
                    case "fa":
                        this.globalFileFormat = "fasta";

                        break;
                    case "sam":
                        this.globalFileFormat = "sam";

                        break;
                    default:
                        goodExtensions = false;
                        break;
                }
            }
            else {
                switch (this.globalFileFormat) {
                    case "fastq":
                        if (!(fileInputExtension.equals("fastq") || fileInputExtension.equals("fq"))) {
                            goodExtensions = false;
                        }
                        break;
                    case "fasta":
                        if (!(fileInputExtension.equals("fasta") || fileInputExtension.equals("fa"))) {
                            goodExtensions = false;
                        }
                        break;
                    case "sam":
                        if (!(fileInputExtension.equals("sam"))) {
                            goodExtensions = false;
                        }
                        break;
                }
            }
        }
        return goodExtensions;
    }

}
