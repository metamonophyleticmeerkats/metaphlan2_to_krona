package nl.bioinf.metamonophyleticmeerkats.checkers;

import nl.bioinf.metamonophyleticmeerkats.globalutilities.FileAndFolderInputOutput;

import java.util.HashMap;

public class PropertiesChecker {
    /**
     * Validates if required properties are written in the MetaToKronaConfig,
     * these properties are required for setting up the server.
     * An error will be generated if a property is missing and you are not allowed to run the program.
     */
    public static final HashMap<String, String> checkMetaToKronaConfig(HashMap<String, String> metaToKronaConfig) {
        if (metaToKronaConfig != null) {
            // Acquire MetaPhlAn 2 python script directory from properties file.
            String metaphlanTwoDir = metaToKronaConfig.get("metaphlanTwoDir");
            // Acquire Krona ImportText pearl script directory from properties file.
            String kronaImportTextScript = metaToKronaConfig.get("kronaImportTextScript");
            // Set folder where job files will be submitted to,
            // each job will create a separate directory in this upload folder.
            String uploadFolder = metaToKronaConfig.get("uploadFolder");
            // Acquire Bowtie2 Database location.
            String bowtieDatabase = metaToKronaConfig.get("bowtieDatabase");
            // Set a maximum file upload size to the upload folder.
            String maxUploadSizeInBytes = metaToKronaConfig.get("maxUploadSizeInBytes");
            // Set the folder length name. The longer the name the more jobs can be submitted.
            // Linux has a max length of 255 characters other operating systems this is different!
            String hashSize = metaToKronaConfig.get("hashSize");
            // A random hash of characters if no job name is chosen.
            String jobHashSize = metaToKronaConfig.get("jobHashSize");
            // Amount of cpu cores which can be utilized by MetaPhlAn 2.
            String coreCount = metaToKronaConfig.get("coreCount");
            // Time how long the result of job remains after it was finished.
            String expirationTimeInSeconds = metaToKronaConfig.get("expirationTimeInSeconds");
            // Time interval of which the database is cleaned and data is removed.
            String databaseScrubberIntervalMillis = metaToKronaConfig.get("databaseScrubberIntervalMillis");
            // Domain on which the tool runs.
            String domainName = metaToKronaConfig.get("domainName");
            // Set port on which the tool has to run.
            String port = metaToKronaConfig.get("port");
            // Internet protocol used.
            String protocol = metaToKronaConfig.get("protocol");

            // Validation of the properties to see if they are specified in the MetaToKronaConfig properties file.

            // metaphlanTwoDir
            if (metaphlanTwoDir == null || !FileAndFolderInputOutput.checkIfExists(metaphlanTwoDir)) {
                return null;
            }
            // kronaImportTextScript
            else if (kronaImportTextScript == null || !FileAndFolderInputOutput.checkIfExists(kronaImportTextScript)) {
                return null;
            }
            // uploadFolder
            else if (uploadFolder == null || !FileAndFolderInputOutput.checkIfExists(uploadFolder)) {
                return null;
            }
            // bowtieDatabase
            else if (bowtieDatabase == null) {
                return null;
            }
            else if (!bowtieDatabase.equals("DEFAULT") && !FileAndFolderInputOutput.checkIfExists(bowtieDatabase)) {
                return null;
            }
            // maxUploadSizeInBytes
            else if (maxUploadSizeInBytes == null || !hashSize.matches("\\d+") ||
                    !(Long.parseLong(maxUploadSizeInBytes) >= 1024)) {
                return null;
            }

            // hashSize
            else if (hashSize == null || !hashSize.matches("\\d+") ||
            !isLongInRange((Long.parseLong(hashSize)), 16, 255)) {
                return null;
            }
            // jobHashSize
            else if (jobHashSize == null || !jobHashSize.matches("\\d+") ||
            !isLongInRange((Long.parseLong(jobHashSize)), 1, 255)) {
                return null;
            }
            // coreCount
            else if (coreCount == null || !coreCount.matches("\\d+") || !(Integer.parseInt(coreCount) >= 1)) {
                return null;
            }
            // expirationTimeInSeconds
            else if (expirationTimeInSeconds == null || !expirationTimeInSeconds.matches("\\d+") ||
                    !(Long.parseLong(expirationTimeInSeconds) >= 300)) {
                return null;
            }
            // databaseScrubberIntervalMillis
            else if (databaseScrubberIntervalMillis == null || !databaseScrubberIntervalMillis.matches("\\d+") ||
                    !(Long.parseLong(databaseScrubberIntervalMillis) >= 1000)) {
                return null;
            }
            // domainName
            else if (domainName == null || domainName.isEmpty()) {
                return null;
            }
            // port
            else if (port == null || !port.matches("\\d+")) {
                return null;
            }
            // protocol
            else if (protocol == null || !(protocol.equals("http") || protocol.equals("https"))) {
                return null;
            }
            else {
                return metaToKronaConfig;
            }
        }
        else {
            return null;
        }
    }

    /**
     * Validates if the requested properties and their values are specified,
     * if not available the web app is not going to run.
     */
    public static final HashMap<String, String> checkMetaToKronaMailConfig(
            HashMap<String, String> metaToKronaMailConfig) {
        if (metaToKronaMailConfig != null) {
            // Set domain host of the email-address.
            String smtpHost = metaToKronaMailConfig.get("smtpHost");
            // Set username for mail address.
            String username = metaToKronaMailConfig.get("username");
            // Set password for mail address.
            String password = metaToKronaMailConfig.get("password");
            // Set email-address for sender.
            String internetAddress = metaToKronaMailConfig.get("internetAddress");
            if (smtpHost == null || smtpHost.isEmpty()) {
                return null;
            }
            else if (username == null || username.isEmpty()) {
                return null;
            }
            else if (password == null || password.isEmpty()) {
                return null;
            }
            else if (internetAddress == null || !internetAddress.matches("([^,@]+@[^,@]+\\.[^,@]+)")) {
                return null;
            }
            else {
                return metaToKronaMailConfig;
            }
        }
        else {
            return null;
        }
    }

    /**
     * Checks whether the long value is within in a certain range.
     */
    private static final boolean isLongInRange(long theLong, long min, long max) {
        return theLong >= min && theLong <= max;
    }
}
