package nl.bioinf.metamonophyleticmeerkats.pipeline;

import nl.bioinf.metamonophyleticmeerkats.database.DatabaseEntry;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.*;

import java.io.IOException;
import java.util.ArrayList;
/**
 * Executes the whole pipeline, if multiple files are available they are concatenated.
 * MetaPhlAn 2 is executed and its results are given to the metaphlan_conversion.py.
 * The results of the conversion are given to Krona which makes krona.html of the file.
 */
public class PipelineRunner implements FormattedTimeInterface, WebappPaths, ConfigProperties {
    /**
     * Execute the pipeline.
     */
    public static final void runPipeline(DatabaseEntry checkedDbEntry) {
        // Grab necessary stuff from the databaseEntry object.
        String hash = checkedDbEntry.getHash();
        // Request files.
        ArrayList<String> fileInputs = checkedDbEntry.getFileInputs();
        // Request file type (fasta, fastq, sam).
        String metaInputType = checkedDbEntry.getFileType();
        // Remove spaces for good measure. (processBuilder will flip out otherwise, necessary for concatting.)
        hash = hash.replace(" ", "");
        // Also remove spaces for each file input.
        for (int i = 0; i < fileInputs.size(); i++) {
            String inputFile = fileInputs.get(i);
            inputFile = inputFile.replace(" ", "");
            fileInputs.set(i, inputFile);
        }
        // Define paths from the config.
        // MetaPhlAn 2 directory.
        String metaphlanDir = metaToKronaConfigProperties.get("metaphlanTwoDir")+"/";
        // Krona directory.
        String kronaImportText = metaToKronaConfigProperties.get("kronaImportTextScript");
        // Which folders the results should be given to.
        String outputFolder = metaToKronaOutputFolder + "/" + hash + "/";
        // Amount of CPU cores which can be uitilized by MetaPhlAn 2.(bowtie2)
        int metaCores = Integer.parseInt(metaToKronaConfigProperties.get("coreCount"));
        // Make child path directories under output.
        String metaOutputfolder = outputFolder + "/metaphlan2output";
        String kronaOutputfolder = outputFolder + "/krona_output";
        // Inputfiles (concatfile).
        String finalFastqInput;
        // Output file specifications.
        String bowtiemetaOutfile = metaOutputfolder + "/metagenome.bowtie2.bz2";
        String metaOutfile = metaOutputfolder + "/profiled_metagenome.txt";
        String metatokronaOutputfile = outputFolder + "/meta2krona_output.txt";
        String concatOutputfile;
        // Check what kind of concat file to make.
        switch (metaInputType) {
            case "fastq":
                concatOutputfile = outputFolder + "/concat.fastq";
                break;
            case "fasta":
                concatOutputfile = outputFolder + "/concat.fasta";
                break;
            case "sam":
                concatOutputfile = outputFolder + "/concat.sam";
                break;
            default:
                concatOutputfile = outputFolder + "/concat.txt";
                break;
        }
        // Specify krona html output path.
        String kronaOutputfile = kronaOutputfolder +"/krona_output.html";

        try {
            // Make necessary output folders.
            FileAndFolderInputOutput.createFolder(outputFolder);
            FileAndFolderInputOutput.createFolder(metaOutputfolder);
            FileAndFolderInputOutput.createFolder(kronaOutputfolder);

            // Define ProcessSerializer.
            ProcessSerializer pipelinePrograms = new ProcessSerializer();
            // Concat the files if necessary.
            boolean concatNecessary = false;
            // More than one file it will be concat.
            if (fileInputs.size() > 1) {
                // Build command for file_concatter script.
                ArrayList<String> concatArgs = new ArrayList<>();
                concatArgs.add("python3");
                concatArgs.add(webroot + "/WEB-INF/pythonscripts/file_concatter.py");
                concatArgs.addAll(fileInputs);
                concatArgs.add(concatOutputfile);
                // Execute Python file_conccater script.
                ProcessBuilder concatProcess = new ProcessBuilder(concatArgs);
                pipelinePrograms.addProcess(concatProcess, "concatter");
                finalFastqInput = concatOutputfile;
                concatNecessary = true;
            } else {
                // In case there was only 1 file.
                finalFastqInput = fileInputs.get(0);
            }
            // Make MetaPhlAn 2 process and add it to the serializer.
            // Make file for recording parameters.
            String metaExecOut = outputFolder+"/metaphlan2_executable.txt";
            MetaphlanTwoArgumentParser meta = new MetaphlanTwoArgumentParser(metaphlanDir);
            // Combine commands and build execution string for MetaPhlAn 2.
            String metaExecstring = meta.metaphlanTwoExecstring(finalFastqInput, bowtiemetaOutfile, metaCores,
                    metaInputType, metaOutfile, checkedDbEntry, metaExecOut);
            // Make bash script to bypass errors generated by MetaPhlAn 2 which causes errors in the pipeline.
            BashScript metaphlantwo = new BashScript(webroot+"/WEB-INF/tempbashscripts/metaphlantemp.sh", metaExecstring);

            // Add MetaPhlAn 2 process to the pipeline.
            ProcessBuilder metaphlantwoProcess = metaphlantwo.MakeProcess();
            pipelinePrograms.addProcess(metaphlantwoProcess, "metaphlan2");

            // Add the process of converting MetaPhlAn 2 data to Krona readable data.
            ProcessBuilder metaToKronaProcess = new ProcessBuilder("python3", webroot+"/WEB-INF/pythonscripts/metaphlan_conversion.py", "-p", metaOutfile, "-k", metatokronaOutputfile);
            pipelinePrograms.addProcess(metaToKronaProcess, "metatokrona");

            // Add Krona to the pipeline.
            ProcessBuilder kronaProcess = new ProcessBuilder(kronaImportText, "-o", kronaOutputfile, metatokronaOutputfile);
            pipelinePrograms.addProcess(kronaProcess, "krona");

            // Run all processes serialized.
            // First specify log files to catch errors and process.
            String logOut = outputFolder+"/log.txt";
            pipelinePrograms.runProcesses(logOut);

            // Clean up the concatenated file.
            metaphlantwo.removeScript();
            if (concatNecessary) {
                FileAndFolderInputOutput.deleteFile(concatOutputfile);
            }


        }
        catch(IOException ioE){
            // IO errors are send to the log file.
                System.out.println(formattedTime +"IO error, some files or folder may not exist, or could not be " +
                        "created.");
            System.out.println(ioE);
            ioE.printStackTrace();
            }

        catch (InterruptedException inE) {
            // Interruption errors are send to the log file.
            System.out.println(formattedTime +"You stopped the process");
            System.out.println(inE);
            inE.printStackTrace();
        }
    }


}
