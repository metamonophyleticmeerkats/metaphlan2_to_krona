package nl.bioinf.metamonophyleticmeerkats.pipeline;

import nl.bioinf.metamonophyleticmeerkats.database.DatabaseEntry;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.ConfigProperties;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
/**
 * Parse al arguments which will then be given to MetaPhlAn 2 in a String.
 */
class MetaphlanTwoArgumentParser implements FormattedTimeInterface, ConfigProperties {
    // MetaPhlAn 2 install directory.
    private String metaphlandir;
    // Standard procedure to launch MetaPhlAn 2 trough Python3.
    private String basePython;
    /**
     * Make objects which make the launch of MetaPhlAn 2 possible.
     */
    MetaphlanTwoArgumentParser(String metaphlandir) {
        this.metaphlandir = metaphlandir;
        this.basePython = "python3 "+this.metaphlandir+"/metaphlan2.py ";
    }
    /**
     * Builds the String which is going to be executed by MetaPhlAn 2.
     */
    String metaphlanTwoExecstring(String inputFile, String bowtietwoOut, int nproc, String inputType, String outfile,
                                  DatabaseEntry checkedDbEntry, String metaphlanExecStorageFile) throws IOException {
        // Make the base executable string, the minimum amount necessary for the program to run, add fill the String
        // later on by formatting the String.
        String baseExecstring = this.basePython + inputFile +" --bowtie2out "+ bowtietwoOut +" --nproc "+
                Integer.toString(nproc)+" --input_type "+ inputType +" %s > "+outfile;
        String storageString = "python3 metaphlan2.py [inputfile] [bowtie2out] --nproc " + Integer.toString(nproc) +
                " --input_type "+ inputType +" %s > [outfile]";
        String optionalInputs = "";

        // Unpack the database Entry and fill in the parameters.
        String markerFileInput = checkedDbEntry.getMarkerFileInput();
        String btPs = checkedDbEntry.getBtPs();
        String taxLev = checkedDbEntry.getTaxLev();
        String minCuLen = checkedDbEntry.getMinCuLen();
        boolean ignoreVirusses = checkedDbEntry.getIgnoreVirusses();
        boolean ignoreEukaryotes = checkedDbEntry.getIgnoreEukaryotes();
        boolean ignoreBacteria = checkedDbEntry.getIgnoreBacteria();
        boolean ignoreArchaea = checkedDbEntry.getIgnoreArchaea();
        String statQ =  checkedDbEntry.getStatQ();
        boolean avoidDisQM = checkedDbEntry.getAvoidDisQM();
        String stat = checkedDbEntry.getStat();
        String analysisType = checkedDbEntry.getAnalysisType();
        String nReads = checkedDbEntry.getnReads();
        String presTH = checkedDbEntry.getPresTH();
        String clade = checkedDbEntry.getClade();
        String minAB = checkedDbEntry.getMinAB();
        String minAlignmentLen = checkedDbEntry.getMinAlignmentLen();

        // Now check every single input if it is defined or not, if it is, add it to the exec string.
        if (!isThisNull(minCuLen)) {
            optionalInputs += "--min_cu_len "+minCuLen+" ";
        }
        if (!isThisNull(btPs)) {
            optionalInputs += "--bt2_ps "+btPs+" ";
        }
        if (!isThisNull(taxLev)) {
            optionalInputs += "--tax_lev "+taxLev+" ";
        }
        if (ignoreVirusses) {
            optionalInputs += "--ignore_viruses ";
        }
        if (ignoreEukaryotes) {
            optionalInputs += "--ignore_eukaryotes ";
        }
        if (ignoreBacteria) {
            optionalInputs += "--ignore_bacteria ";
        }
        if (ignoreArchaea) {
            optionalInputs += "--ignore_archaea ";
        }
        if (avoidDisQM) {
            optionalInputs += "--avoid_disqm ";
        }
        if (!isThisNull(statQ)) {
            optionalInputs += "--stat_q "+statQ+" ";
        }
        if (!isThisNull(stat)) {
            optionalInputs += "--stat "+stat+" ";
        }
        if (!isThisNull(analysisType)) {
            optionalInputs += "-t "+analysisType+" ";
        }
        if (!isThisNull(nReads)) {
            optionalInputs += "--nreads "+nReads+" ";
        }
        if (!isThisNull(presTH)) {
            optionalInputs += "--pres_th "+presTH+" ";
        }
        if (!isThisNull(clade)) {
            optionalInputs += "--clade "+clade+" ";
        }
        if (!isThisNull(minAB)) {
            optionalInputs += "--min_ab "+minAB+" ";
        }
        if (!isThisNull(minAlignmentLen)) {
            optionalInputs += "--min_alignment_len "+minAlignmentLen+" ";
        }

        PrintStream stdout = System.out;
        // Log to file.
        System.setOut(new PrintStream(new FileOutputStream(metaphlanExecStorageFile, true)));
        System.out.println(String.format(storageString, optionalInputs+" --ignore_markrs [markerFile]"));
        // Set stream back to normal System out.
        System.setOut(stdout);
        System.out.println(formattedTime +String.format(baseExecstring, optionalInputs));
        // Add marker file input at the end so no full paths are logged for the user.
        // If no marker file is specified then add the --ignore_markers option.
        if (!isThisNull(markerFileInput)) {
            optionalInputs += "--ignore_markers "+markerFileInput+" ";
        }
        // Also add custom bowtie database if defined.
        if (!metaToKronaConfigProperties.get("bowtieDatabase").equals("DEFAULT")) {
            optionalInputs += "--bowtie2db "+metaToKronaConfigProperties.get("bowtieDatabase")+" ";
        }
        // Give back the whole execution String.
        return String.format(baseExecstring, optionalInputs);
    }
    /**
     * Makes an object null.
     */
    private boolean isThisNull(Object rawInput) {
        return (rawInput == null);
    }
}
