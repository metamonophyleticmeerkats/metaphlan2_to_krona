package nl.bioinf.metamonophyleticmeerkats.servlets;

import nl.bioinf.metamonophyleticmeerkats.database.DatabaseScrubberBackgroundProcess;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * This is a weblistener and starts the database scrubber in the background.
 */
@WebListener
public class DatabaseScrubberListener implements ServletContextListener {
    // make an executor service and start the database scrubber / checker.
    private ExecutorService executor;
    /**
     * starts the database checker
     */
    public void contextInitialized(ServletContextEvent event) {
        executor = Executors.newSingleThreadExecutor();
        executor.submit(new DatabaseScrubberBackgroundProcess()); // Task should implement Runnable so it can be started
    }
    /**
     * Shuts down the web listener properly so tomcat can properly exit
     */
    public void contextDestroyed(ServletContextEvent event) {
        executor.shutdown();
    }

}