package nl.bioinf.metamonophyleticmeerkats.servlets;

import nl.bioinf.metamonophyleticmeerkats.database.DatabaseManagement;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FileAndFolderInputOutput;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.WebappPaths;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * This class handles the result page of this webapp
 */
@WebServlet(name = "ResultServlet", urlPatterns = "/result")
public class ResultServlet extends HttpServlet implements WebappPaths, FormattedTimeInterface {

    /**
     * This function handles a post request, since it would be invalid for this page, the standard message stays set
     * and user does not get their output.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        // before doing anything, check if configs and paths are correct. and make standard output folder
        if (!FileAndFolderInputOutput.initiateWebapp()) {
            request.setAttribute("faultyConfigs", "CRITICAL: webapp root is somehow missing " +
                    "or configs are not correctly configured / missing! The configs need to be in the " +
                    "\"tomcat/conf/\" location. If you do not know what this message means, contact your server " +
                    "administrator.");
        }
        // do not set any values on a post request.
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        view.forward(request, response);
    }
    /**
     * This function handles a get request and sets the correct page if hash is valid with the get request.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        // before doing anything, check if configs and paths are correct. and make standard output folder
        if (!FileAndFolderInputOutput.initiateWebapp()) {
            request.setAttribute("faultyConfigs", "CRITICAL: webapp root is somehow missing " +
                    "or configs are not correctly configured / missing! The configs need to be in the " +
                    "\"tomcat/conf/\" location. If you do not know what this message means, contact your server " +
                    "administrator.");
        }
        // with a get, try get hash and query database
        else {
            String hash = request.getParameter("hash");
            String jobName = request.getParameter("job");
            // if hash and job does not seem empty, query the database for the hash and see if it exists in db as
            // finished
            if (hash != null && !hash.isEmpty() && !hash.matches("[\\\\\\\";(){}\\'?%\\[\\]=\\* ]") &&
                    jobName != null && !jobName.isEmpty() &&
                    !jobName.matches("[\\\\\\\";(){}\\'?%\\[\\]=\\* ]")) {
                String databaseLocation = webroot + "/WEB-INF/database/MetaToKrona.db";
                try {
                    DatabaseManagement databaseConnection = new DatabaseManagement(databaseLocation);
                    if (databaseConnection.requestFinishedJob(hash)) {
                        // if everything is ok, proceed making view.
                        request.setAttribute("javascript", "<script src=\"js/resultPage.js\"></script>");
                        request.setAttribute("hash", hash);
                        request.setAttribute("job", jobName);
                    }
                    // if the hash is not correct, standard message stays in jsp telling the user that they either
                    // do not have javascript enabled or that their output does not exists (anymore).
                }
                // if something went wrong, let the user know.
                catch (SQLException SeCon) {
                    System.out.println(formattedTime +"Connection with database failed...");
                    System.out.println(formattedTime+SeCon);
                    SeCon.printStackTrace();
                }
                catch (ClassNotFoundException cnfe) {
                    System.out.println(formattedTime +"Something went wrong when loading sqlite library, please check " +
                            "your installation.");
                    System.out.println(formattedTime +cnfe);
                    cnfe.printStackTrace();
                }
            }
        }
        // forward the view after setting all the attributes.
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        view.forward(request, response);
    }
}
