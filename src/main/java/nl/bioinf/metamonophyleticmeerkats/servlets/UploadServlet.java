package nl.bioinf.metamonophyleticmeerkats.servlets;

import nl.bioinf.metamonophyleticmeerkats.checkers.HtmlInputParser;
import nl.bioinf.metamonophyleticmeerkats.database.DatabaseManagement;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * This class handles everything on the upload page.
 */
@MultipartConfig()
@WebServlet(name = "UploadServlet", urlPatterns = "/upload")
public class UploadServlet extends HttpServlet implements FormattedTimeInterface, WebappPaths, ConfigProperties {
    private ArrayList<String> queueNames = new ArrayList<>();
    private ArrayList<String> queueNamesRunning = new ArrayList<>();
    /**
     * This function handles a post request from the upload page.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        // before doing anything, check if configs and paths are correct. and make standard output folder
        if (!FileAndFolderInputOutput.initiateWebapp()) {
            request.setAttribute("faultyConfigs", "CRITICAL%3A%20webapp%20root%20is%20somehow%20missing%20or" +
                    "%20configs%20are%20not%20correctly%20configured%20%2F%20missing!%20The%20configs%20need%20to%20b" +
                    "e%20in%20the%20%22tomcat%2Fconf%2F%22%20location.%20If%20you%20do%20not%20know%20what%20this%20m" +
                    "essage%20means%2C%20contact%20your%20server%20administrator.");
        }
        else {
            // if everything is ok, continue with page, load in javascript and set max upload size for it.
            request.setAttribute("maxUploadSizeInBytes", metaToKronaConfigProperties.get("maxUploadSizeInBytes"));
            request.setAttribute("javascript", "<script src=\"js/uploadPage.js\"></script>");
            // get the submission date
            long submissionDate = System.currentTimeMillis() / 1000L;
            try {
                // Read metaToKronaConfigProperties necessary for hash etc.
                String uploadRootPath = metaToKronaConfigProperties.get("uploadFolder");
                int hashsize = Integer.valueOf(metaToKronaConfigProperties.get("hashSize"));
                int jobHashSize = Integer.valueOf(metaToKronaConfigProperties.get("jobHashSize"));
                String databaseLocation = webroot + "/WEB-INF/database/MetaToKrona.db";
                long expirationTime = Long.parseLong(metaToKronaConfigProperties.get("expirationTimeInSeconds"));

                // make hash and folder
                String hash = "";
                String uploadPath = "";
                // check for duplicate hash, try generating a new one if it already exists.
                boolean duplicateHash = true;
                while (duplicateHash) {
                    HashGenerator hashgen = new HashGenerator();
                    hash = hashgen.generateHash(hashsize);
                    String outputPath = webroot+"/outputs/"+hash+"/";
                    uploadPath = uploadRootPath+"/"+hash+"/";
                    boolean hashExistsAlready = FileAndFolderInputOutput.checkIfExists(outputPath) ||
                            FileAndFolderInputOutput.checkIfExists(uploadPath);
                    if (!hashExistsAlready) {
                        duplicateHash = false;
                    }
                }

                // specify marker upload path
                String markerUploadPath = uploadPath+"/markers/";
                // Read in input files and copy them to the upload location.
                ArrayList<String> fileInputs = FileAndFolderInputOutput.fileUploader(request, uploadPath,
                        "file");
                // Read in the marker file if it is given
                String markerFileInput = FileAndFolderInputOutput.markerFileUploader(request, markerUploadPath,
                        "fileMarker");

                // Read in other attributes, check them, then submit to database.
                if (!fileInputs.isEmpty()) {
                    HtmlInputParser htmlInputParser = new HtmlInputParser(hash, request, fileInputs, markerFileInput,
                            submissionDate, expirationTime, jobHashSize);
                    request = htmlInputParser.checkAllInputsAndSubmit(databaseLocation);
                    // also set the tags for the running and queued jobs
                    manageFrontPageQueue(databaseLocation);
                    request.setAttribute("jobsRunning", queueNamesRunning);
                    request.setAttribute("jobsadded", queueNames);
                }
                else {
                    // if something went wrong, let the user know
                    request.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%2C%20contact" +
                            "%20your%20server%20administrator%20if%20this%20persists..");
                }
            }
            catch (IOException ioex) {
                // let user know something went wrong
                System.out.println(formattedTime +"failed to get configuration file at; \""+webroot+
                        "/WEB-INF/configs/config.properties\" or upload failed.");
                System.out.println(ioex);
                ioex.printStackTrace();
                request.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%2C%20contact%20" +
                        "your%20server%20administrator%20if%20this%20persists.");
            }
            catch (ServletException serex) {
                // let user know something went wrong
                System.out.println(formattedTime +"Something went wrong while forwarding the view.");
                System.out.println(serex);
                serex.printStackTrace();
                request.setAttribute("uploadstate", "Your%20job%20has%20failed%20to%20submit%2C%20contact%20" +
                        "your%20server%20administrator%20if%20this%20persists.");
            }
        }

        // do a post-redirect-get pattern (give all necessary attributes to get.)
        // because all the attributes need to be put in a link, it is encoded for a url. This is why it looks so dodgy.
        response.sendRedirect("upload?"+
                "uploadstate="+request.getAttribute("uploadstate")+"&"+
                "faultyConfigs="+request.getAttribute("faultyConfigs")+"&"+
                "faultyFiles="+request.getAttribute("faultyFiles")+"&"+
                "faultyJobname="+request.getAttribute("faultyJobname")+"&"+
                "faultyEmails="+request.getAttribute("faultyEmails")+"&"+
                "faultyClade="+request.getAttribute("faultyClade")+"&"+
                "faultyBtps="+request.getAttribute("faultyBtps")+"&"+
                "faultyTaxLev="+request.getAttribute("faultyTaxLev")+"&"+
                "faultyStat="+request.getAttribute("faultyStat")+"&"+
                "faultyAnalysisType="+request.getAttribute("faultyAnalysisType")+"&"+
                "faultyMinCuLen="+request.getAttribute("faultyMinCuLen")+"&"+
                "faultyMinAlignmentLen="+request.getAttribute("faultyMinAlignmentLen")+"&"+
                "faultyminNreads="+request.getAttribute("faultyminNreads")+"&"+
                "faultyStatQ="+request.getAttribute("faultyStatQ")+"&"+
                "faultyMinAB="+request.getAttribute("faultyMinAB")+"&"+
                "faultyPresTH="+request.getAttribute("faultyPresTH")
        );
    }
    /**
     * This function handles a get request from the upload page.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        // before doing anything, check if configs and paths are correct. and make standard output folder
        if (!FileAndFolderInputOutput.initiateWebapp()) {
            request.setAttribute("faultyConfigs", "CRITICAL: webapp root is somehow missing " +
                    "or configs are not correctly configured / missing! The configs need to be in the " +
                    "\"tomcat/conf/\" location. If you do not know what this message means, contact your server " +
                    "administrator.");
        }
        else {
            // first unpack get from post and set the attributes for it.
            request = unpackPostRedirectGet(request);
            // then set other, always present attributes.
            manageFrontPageQueue(webroot + "/WEB-INF/database/MetaToKrona.db");
            request.setAttribute("maxUploadSizeInBytes", metaToKronaConfigProperties.get("maxUploadSizeInBytes"));
            request.setAttribute("javascript", "<script src=\"js/uploadPage.js\"></script>");
            request.setAttribute("jobsRunning", queueNamesRunning);
            request.setAttribute("jobsadded", queueNames);
        }
        // forward the view.
        RequestDispatcher view = request.getRequestDispatcher("upload.jsp");
        view.forward(request, response);
    }
    /**
     * This function manages the front page queue and updates both lists depending on what's present in the database.
     */
    private void manageFrontPageQueue(String databaseLocation) {
        try {
            // try to connect to database and request the remaining jobs for both running and non-running jobs.
            DatabaseManagement receiveDatabaseQueue = new DatabaseManagement(databaseLocation);
            ArrayList<String> remainingQueueJobs = receiveDatabaseQueue.requestRemainingJobs(0);
            ArrayList<String> currentlyRunningJob = receiveDatabaseQueue.requestRemainingJobs(1);
            // clear both lists and fill them up with the updated information.
            queueNames.clear();
            queueNamesRunning.clear();
            queueNames.addAll(remainingQueueJobs);
            queueNamesRunning.addAll(currentlyRunningJob);
        }
        // if something went wrong let the user know.
        catch (SQLException SeCon) {
            System.out.println(formattedTime +"Connection with database failed...");
            System.out.println(formattedTime+SeCon);
            SeCon.printStackTrace();
        }
        catch (ClassNotFoundException cnfe) {
            System.out.println(formattedTime +"Something went wrong when loading sqlite library, please check your " +
                    "installation.");
            System.out.println(formattedTime +cnfe);
            cnfe.printStackTrace();
        }
    }

    /**
     * unpacks post-redirect-get pattern. If a post request was made, it will be forwarded to the get, and all the
     * user messages need to be unpacked.
     */
    private HttpServletRequest unpackPostRedirectGet(HttpServletRequest request){
        request = setAttributeIfNotNull(request, "uploadstate");
        request = setAttributeIfNotNull(request, "faultyConfigs");
        request = setAttributeIfNotNull(request, "faultyFiles");
        request = setAttributeIfNotNull(request, "faultyJobname");
        request = setAttributeIfNotNull(request, "faultyEmails");
        request = setAttributeIfNotNull(request, "faultyClade");
        request = setAttributeIfNotNull(request, "faultyBtps");
        request = setAttributeIfNotNull(request, "faultyTaxLev");
        request = setAttributeIfNotNull(request, "faultyStat");
        request = setAttributeIfNotNull(request, "faultyAnalysisType");
        request = setAttributeIfNotNull(request, "faultyMinCuLen");
        request = setAttributeIfNotNull(request, "faultyMinAlignmentLen");
        request = setAttributeIfNotNull(request, "faultyminNreads");
        request = setAttributeIfNotNull(request, "faultyStatQ");
        request = setAttributeIfNotNull(request, "faultyMinAB");
        request = setAttributeIfNotNull(request, "faultyPresTH");

        return request;
    }
    /**
     * Do a check for each post-redirect-get attribute if it is defined correctly. This way, the user won't get weird
     * errors or messages in the web page.
     */
    private HttpServletRequest setAttributeIfNotNull(HttpServletRequest request, String attributeName) {
        if (request.getParameter(attributeName) != null && !request.getParameter(attributeName).equals("null")) {
            request.setAttribute(attributeName, request.getParameter(attributeName));
        }
        return request;
    }
}
