package nl.bioinf.metamonophyleticmeerkats.database;

import java.util.ArrayList;

public class DatabaseEntry {
    private String hash;
    private ArrayList<String> fileInputs;
    private String markerFileInput;
    private String submissionDate;
    private String expirationTime;
    private String jobName;
    private String emails;
    private String btPs;
    private String taxLev;
    private String minCuLen;
    private String minAlignmentLen;
    private boolean ignoreVirusses;
    private boolean ignoreEukaryotes;
    private boolean ignoreBacteria;
    private boolean ignoreArchaea;
    private String statQ;
    private boolean avoidDisQM;
    private String stat;
    private String analysisType;
    private String nReads;
    private String presTH;
    private String clade;
    private String minAB;
    private String fileType;

    /**
     * Instantiates a databaseEntry object which contains all the inputs of from webpage after being validated by the
     * HtmlInputParser.
     * The same object is made twice, the first time is after the submission of a valid job,
     * the second time is when the job is requested from the database and the inputs are given to MetaPhlAn 2
     * to process.
     */
    public DatabaseEntry(String hash, ArrayList<String> fileInputs, String markerFileInput, String submissionDate,
                         String expirationTime, String jobName, String emails, String btPs, String taxLev,
                         String minCuLen, String minAlignmentLen, boolean ignoreArchaea, boolean ignoreBacteria,
                         boolean ignoreEukaryotes, boolean ignoreVirusses, String statQ, boolean avoidDisQM,
                         String stat, String analysisType, String nReads, String presTH, String clade, String minAB,
                         String fileType) {
        this.hash = hash;
        this.fileInputs = fileInputs;
        this.markerFileInput = markerFileInput;
        this.submissionDate = submissionDate;
        this.expirationTime = expirationTime;
        this.jobName = jobName;
        this.emails = emails;
        this.btPs = btPs;
        this.taxLev = taxLev;
        this.minCuLen = minCuLen;
        this.minAlignmentLen = minAlignmentLen;
        this.ignoreVirusses = ignoreVirusses;
        this.ignoreEukaryotes = ignoreEukaryotes;
        this.ignoreBacteria = ignoreBacteria;
        this.ignoreArchaea = ignoreArchaea;
        this.statQ = statQ;
        this.avoidDisQM = avoidDisQM;
        this.stat = stat;
        this.analysisType = analysisType;
        this.nReads = nReads;
        this.presTH = presTH;
        this.clade = clade;
        this.minAB = minAB;
        this.fileType = fileType;
    }

    public String getHash() {
        return hash;
    }

    public ArrayList<String> getFileInputs() {
        return fileInputs;
    }

    public String getMarkerFileInput() {
        return markerFileInput;
    }

    String getSubmissionDate() {
        return submissionDate;
    }

    String getExpirationTime() {
        return expirationTime;
    }

    String getJobName() {
        return jobName;
    }

    String getEmails() {
        return emails;
    }

    public String getBtPs() {
        return btPs;
    }

    public String getTaxLev() {
        return taxLev;
    }

    public String getMinCuLen() {
        return minCuLen;
    }

    public String getMinAlignmentLen() {
        return minAlignmentLen;
    }

    public boolean getIgnoreVirusses() {
        return ignoreVirusses;
    }

    public boolean getIgnoreEukaryotes() {
        return ignoreEukaryotes;
    }

    public boolean getIgnoreBacteria() {
        return ignoreBacteria;
    }

    public boolean getIgnoreArchaea() {
        return ignoreArchaea;
    }

    public String getStatQ() {
        return statQ;
    }

    public boolean getAvoidDisQM() {
        return avoidDisQM;
    }

    public String getStat() {
        return stat;
    }

    public String getAnalysisType() {
        return analysisType;
    }

    public String getnReads() {
        return nReads;
    }

    public String getPresTH() {
        return presTH;
    }

    public String getClade() {
        return clade;
    }

    public String getMinAB() {
        return minAB;
    }

    public String getFileType() {
        return fileType;
    }

    /**
     * Makes a string representation of the object possible.
     */
    @Override
    public String toString() {
        String fullString = "";
        fullString += this.hash+"\n";
        fullString += this.fileInputs+"\n";
        fullString += this.markerFileInput+"\n";
        fullString += this.submissionDate+"\n";
        fullString += this.expirationTime+"\n";
        fullString += this.jobName+"\n";
        fullString += this.emails+"\n";
        fullString += this.btPs+"\n";
        fullString += this.taxLev+"\n";
        fullString += this.minCuLen+"\n";
        fullString += this.minAlignmentLen+"\n";
        fullString += this.ignoreVirusses+"\n";
        fullString += this.ignoreEukaryotes+"\n";
        fullString += this.ignoreBacteria+"\n";
        fullString += this.ignoreArchaea+"\n";
        fullString += this.statQ+"\n";
        fullString += this.avoidDisQM+"\n";
        fullString += this.stat+"\n";
        fullString += this.analysisType+"\n";
        fullString += this.nReads+"\n";
        fullString += this.presTH+"\n";
        fullString += this.clade+"\n";
        fullString += this.minAB+"\n";
        fullString += this.fileType+"\n";
        return fullString;
    }
}

