package nl.bioinf.metamonophyleticmeerkats.database;

import nl.bioinf.metamonophyleticmeerkats.globalutilities.ConfigProperties;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FileAndFolderInputOutput;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.WebappPaths;
import nl.bioinf.metamonophyleticmeerkats.pipeline.PipelineRunner;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

public class DatabaseScrubberBackgroundProcess implements Runnable, FormattedTimeInterface, WebappPaths, ConfigProperties {

    @Override
    public void run() {
        // Before doing anything, check if configs and paths are correct. And make output path if necessary.
        if (!FileAndFolderInputOutput.initiateWebapp()) {
            System.out.println("CRITICAL: webapp root is somehow missing or configs are not correctly configured / " +
                    "missing! The configs need to be in the \"tomcat/conf/\" location. If you do not know what this " +
                    "message means, contact your server administrator.");
        }
        else {
            // Set sys.out to log file for admin.
            PrintStream stdout = System.out;
            PrintStream stderr = System.err;
            try {
                // Write output to log file.
                PrintStream adminLogStream = new PrintStream(new FileOutputStream(webroot+"/WEB-INF/MetaToKrona.log", true));
                System.setOut(adminLogStream);
                System.setErr(adminLogStream);
            } catch (FileNotFoundException e) {
                // Unable to find log file.
                System.out.println(formattedTime +"CRITICAL: Could not redefine output to log!!!");
                System.out.println(formattedTime+e);
                e.printStackTrace();
            }
            // Go and start database scrubbing loop.
            databaseScrubLoop();
            // Set sys.out en err stream back to standard.
            System.setOut(stdout);
            System.setErr(stderr);
        }
    }

    /**
     * This method handles the constant database maintenance with inserting, deleting, updating, and selecting.
     */
    private void databaseScrubLoop(){
        // Load in necessary properties.
        // Time interval at which the scrubber runs.
        long timeout = Long.parseLong(metaToKronaConfigProperties.get("databaseScrubberIntervalMillis"));
        // Database directory.
        String databaseLocation = webroot + "/WEB-INF/database/MetaToKrona.db";
        String protocol = metaToKronaConfigProperties.get("protocol");
        // Port over which the connection is made.
        String port = metaToKronaConfigProperties.get("port");

        // Go into while loop.
        boolean justRanAJob = false;
        boolean bootup = true;
        while (true) {
            try {
                if (bootup) {
                    System.out.println(formattedTime+"STARTING WEBAPP METATOKRONA, SERVER BOOTUP");
                }

                // Don't sleep if a job just finished, instead do another scrub.
                if (!justRanAJob) {
                    //Sleep to reduce the load of the program, otherwise it would do many requests in a short interval.
                    Thread.sleep(timeout);
                }
                // First check if the server has a shutdown requested, if so, break out of the for loop.
                // (this is checked by seeing if localhost is reachable.)
                boolean serverShutdownRequest = pingHost(protocol+"://localhost:"+port);
                if (serverShutdownRequest) {
                    System.out.println(formattedTime +"Detected system shutdown, background listener shutting down!");
                    break;
                }
                // Make new connection to sql database.
                System.out.println(formattedTime +"Scrubbing database..");
                DatabaseManagement databaseBackgroundManager = new DatabaseManagement(databaseLocation);
                // First check for entries that are past their expiration date.
                System.out.println(formattedTime +"Scrubbing away old entries...");
                databaseBackgroundManager.removeOvertimeEntries();
                // Then check for running processes and mark crashed jobs.
                System.out.println(formattedTime +"Scrubbing Crashed or Finished processes...");
                databaseBackgroundManager.scrubFinishedOrCrashedProcesses(bootup, justRanAJob);
                // Afterwards, ask which job is next in the queue run the pipeline.
                System.out.println(formattedTime +"Scrubbing for next jobs.");
                DatabaseEntry databaseEntry = databaseBackgroundManager.reCreateLastDatabaseEntry();
                // Check if nothing was in queue, in this case, object = null
                if (databaseEntry == null) {
                    justRanAJob = false;
                }
                else {
                    // Initiate pipeline job.
                    justRanAJob = true;
                    // First update value in db before actually running the whole pipeline.
                    databaseBackgroundManager.initializeExecutionForFirstInQueue(databaseEntry.getHash());
                    // Run the pipeline.
                    System.out.println(formattedTime+"Started pipeline run!");
                    PipelineRunner.runPipeline(databaseEntry);
                }
                // Disable bootUp parameter to check for crashed jobs at start.
                bootup = false;
                databaseBackgroundManager.closeSQLDBConnection();

            }

            // Multiple catches:
            // Interrupt means that the job could not run because the thread failed,
            // ClassNotFound means most likely that the driver is not available,
            // ,SQL failed is most likely a failed database connection.
            catch (InterruptedException e) {
                justRanAJob = false;
                System.out.println(formattedTime+"Job got interrupted?: "+e);
                e.printStackTrace();
            }
            catch (ClassNotFoundException cnfe) {
                justRanAJob = false;
                System.out.println(formattedTime +"Something went wrong when trying to load sql drivers...");
                System.out.println(formattedTime+cnfe);
                cnfe.printStackTrace();
            }
            catch (SQLException SConE) {
                justRanAJob = false;
                System.out.println(formattedTime +"Connection failed:");
                System.out.println(formattedTime +SConE);
                SConE.printStackTrace();
            }
        }
    }

    /**
     * Tests whether the host is reachable if not the server will be shutdown.
     */
    private static boolean pingHost(String url) {

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            int responseCode = connection.getResponseCode();
            // Not OK. (server shutdown.)
            return responseCode != 200;
        }
        catch (IOException iox) {
            // Connection could not be set up, return true
            return true;
        }

    }
}
