package nl.bioinf.metamonophyleticmeerkats.database;

import nl.bioinf.metamonophyleticmeerkats.globalutilities.ConfigProperties;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FileAndFolderInputOutput;
import nl.bioinf.metamonophyleticmeerkats.globalutilities.FormattedTimeInterface;
import nl.bioinf.metamonophyleticmeerkats.mail.MetaMail;

import javax.mail.Session;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Object for managing the database
 */
public class DatabaseManagement implements FormattedTimeInterface, ConfigProperties {
    private String databaseLocation;
    private Connection connection;
    private Statement statement;

    public DatabaseManagement(String databaseLocation) throws SQLException, ClassNotFoundException{
        this.databaseLocation = databaseLocation;
        // Connection object.
        this.connection = connectToDatabase();
        //Make statement that can be executed or updated.
        this.statement = this.connection.createStatement();
    }
    /**
     * Make connection to the sqlite database.
     */
    private Connection connectToDatabase() throws SQLException, ClassNotFoundException {
        // Force load the sqlite drivers.
        Class.forName("org.sqlite.JDBC");
        return DriverManager.getConnection("jdbc:sqlite:"+this.databaseLocation);
    }
    /**
     * Deletes database entries which are finished` and have a lower unix time than the current time.
     * `(having status finished_running = 1)
     */
    void removeOvertimeEntries() throws SQLException, InterruptedException{
        ResultSet rs = this.statement.executeQuery("SELECT hash FROM QUEUESUBMISSIONS where expiration_date < strftime('%s','now') AND finished_running = 1;");
        // First checks if result set is empty. (cant use .next() because sql only supports; TYPE_FORWARD_ONLY ResultSets)
        if (!rs.next() ) {
            System.out.println(formattedTime +"no data with state overtime.");
        }
        else {
            // because sqlite is so beautiful, we either had to unpack the last query to begin with, or rerun the query like so;
            rs = this.statement.executeQuery("SELECT hash FROM QUEUESUBMISSIONS where expiration_date < strftime('%s','now') AND finished_running = 1;");
            // Acquire the hash from the ResultSet to find the folder with the data and delete the folder recursively.
            String hash = rs.getString("hash");
            String outputFolder = metaToKronaOutputFolder+"/"+hash+"/";
            // Deleting the folder recursively.
            FileAndFolderInputOutput.deleteFolder(outputFolder);
            // If the folder is deleted, delete the entry in the database with the finished job.
            if (!FileAndFolderInputOutput.checkIfExists(outputFolder)) {
                // sql statement to delete from db
                this.statement.executeUpdate("DELETE FROM QUEUESUBMISSIONS WHERE expiration_date < strftime('%s','now') AND finished_running = 1;");
            }
        }
    }
    /**
     * Returns hash and job name for a given hash where the job finished.
     */
    public boolean requestFinishedJob(String hash) throws SQLException {
        // Get the entry which is equal to the hash and that is finished with running.
        ResultSet rsHashes = this.statement.executeQuery("SELECT hash FROM QUEUESUBMISSIONS WHERE hash = \""+hash+"\" AND running = 0 AND finished_running = 1 LIMIT 1;");

        // Add all entries to the ArrayList and test whether the job is finished.(it is only 1)
        ArrayList<String> hashes = new ArrayList<>();
        while (rsHashes.next()) {
            hashes.add(rsHashes.getString("hash"));
        }
        if (hashes.size() > 0) {
            String dbHash = hashes.get(0);
            if (dbHash == null || dbHash.isEmpty()) {
                return false;
            }
            else if (dbHash.equals(hash)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }


    /**
     * Creates an ArrayList of all jobs with the specified running boolean, in the end the results should be merged for the queue.
     */
    public ArrayList<String> requestRemainingJobs(int runningBool)throws SQLException {
        ResultSet rs = this.statement.executeQuery("SELECT jobname FROM QUEUESUBMISSIONS WHERE running = "+runningBool+" AND finished_running = 0;");

        ArrayList<String> inQueue = new ArrayList<>();
        while (rs.next()) {
            inQueue.add(rs.getString("jobName"));
        }
        return inQueue;
    }

    /**
     * Calculate an expiration date based on the expiration time and the current Unix time.
     */
    private long createExperationDate() throws SQLException{
       long calculatedExperationDate;
       ResultSet rs = this.statement.executeQuery("SELECT expiration_time FROM QUEUESUBMISSIONS WHERE running = 1 AND finished_running = 0;");
       calculatedExperationDate = (System.currentTimeMillis() / 1000) + rs.getInt("expiration_time");
       return calculatedExperationDate;

    }

    /**
     * Updates a Finished or Crashed job in the database.
     */
    private void updateFinishedOrCrashedDatabaseEntry(boolean crashed, String emails, String hash, String jobName) throws SQLException, InterruptedException {
        try {
            // Clean up the upload files to save space on the system.
            boolean cleanedUpCorrectly = FileAndFolderInputOutput.cleanUpMetaToKronaFiles(hash, jobName);
            // Do not proceed with updating/cleaning the database entry if the upload files where not removed.
            if (cleanedUpCorrectly) {
                // First part of the statement is constant.
                String startOfStatement = "UPDATE QUEUESUBMISSIONS SET " +
                        "emails = NULL, " +
                        "expiration_date ="+createExperationDate()+",";

                String endOfStatement = "jobname = \"\"," +
                        "file_input = NULL, " +
                        "bt2_ps = NULL, " +
                        "tax_lev = NULL, " +
                        "min_cu_len = NULL, " +
                        "ignore_virusses = NULL, " +
                        "ignore_eukaryotes = NULL, " +
                        "ignore_bacteria = NULL, " +
                        "ignore_archaea = NULL, " +
                        "stat_q = NULL, " +
                        "ignore_markers = NULL, " +
                        "avoid_disqm = NULL, " +
                        "stat = NULL, " +
                        "analysis_type = NULL, " +
                        "nreads = NULL, " +
                        "pres_th = NULL, " +
                        "clade = NULL, " +
                        "min_ab = NULL, " +
                        "file_type = NULL, " +
                        "min_alignment_len = NULL " +
                        "WHERE running = 1 AND finished_running = 0;";

                // Before sending any email, grab the necessary properties to be able to build link.

                String protocol = metaToKronaConfigProperties.get("protocol");
                String domainName = metaToKronaConfigProperties.get("domainName");
                String port = metaToKronaConfigProperties.get("port");

                // In case the process with the job crashed this mail will be sent to the user.
                if (crashed) {
                    MetaMail mm = new MetaMail();
                    // Create new login session.
                    Session mmLoggedin = mm.metaMailLogin();
                    // Send email.
                    mm.metaMailSendMail(mmLoggedin,emails,"[MetaToKrona] Your job \""+jobName+"\" has crashed!",
                            "While running your job, something went wrong. This is either the servers fault or " +
                                    "somehow incorrect inputs made your job crash. Try submit again with same parameters, if " +
                                    "the job still keeps crashing, contact your server administrator. You can find your old " +
                                    "parameters and other log files here: \n"+protocol+"://"+domainName+":"+port+"/outputs/"+hash+
                                    "/"+jobName+".zip");
                    // Remove all sent mail.
                    mm.metaMailRemoveOutgoingMail(mmLoggedin);
                    // Server information that the job was not successful.
                    System.out.println(formattedTime +"Job crashed?");
                    // Update database entry and set it into crashed state.
                    this.statement.executeUpdate(startOfStatement+
                            "running = 1,"+
                            "finished_running = 1,"+
                            endOfStatement);
                }
                // When the job is finished without crashes this mail will be sent.
                else {
                    MetaMail mm = new MetaMail();
                    // Create new login session.
                    Session mmloggedin = mm.metaMailLogin();
                    // Send email.
                    mm.metaMailSendMail(mmloggedin,emails,"[MetaToKrona] Your job \""+jobName+"\" has finished!",
                            "Your job has finished correctly and can be found here: \n"+protocol+"://"+domainName+
                                    ":"+port+"/result?hash="+hash+"&job="+jobName+" \nSometimes, if " +
                                    "incorrect input is given, the krona plot might be blank. If you experience any issues with " +
                                    "this link, contact your server administrator.");
                    mm.metaMailRemoveOutgoingMail(mmloggedin);
                    // Server information that the job was successful
                    System.out.println(formattedTime +"Job finished!");
                    // Update database entry.
                    this.statement.executeUpdate(startOfStatement+
                            "running = 0,"+
                            "finished_running = 1,"+
                            endOfStatement);
                }
            }
        }
        catch (IOException ioex) {
            // Report crash
            System.out.println(formattedTime +"could not delete some files");
            System.out.println(formattedTime+ioex);
            ioex.printStackTrace();
        }
    }

    /**
     * This method removes any crashed processes.
     */
    void scrubFinishedOrCrashedProcesses(boolean bootUp, boolean justFinishedJob) throws SQLException, InterruptedException {
        // Queries for currently running jobs.
        ResultSet runningOrCrashed = this.statement.executeQuery("SELECT * FROM QUEUESUBMISSIONS WHERE running = 1 AND finished_running = 0;");

        // First check if result set is empty. (cant use .next() because sql only supports; TYPE_FORWARD_ONLY ResultSets).
        // Does seem to run
        if (!runningOrCrashed.next() ) {
            System.out.println(formattedTime +"no data with state running.");
        }
        else {
            // Because sqlite is so beautiful, we either had to unpack the last query to begin with, or rerun the query like so.
            runningOrCrashed = this.statement.executeQuery("SELECT * FROM QUEUESUBMISSIONS WHERE running = 1 AND finished_running = 0;");
            // Reconstruct position of krona html.
            String hash = runningOrCrashed.getString("hash");
            String kronaHtmlFile = metaToKronaOutputFolder+"/"+hash+"/krona_output/krona_output.html";
            boolean kronaExists = FileAndFolderInputOutput.checkIfExists(kronaHtmlFile);
            String emails = runningOrCrashed.getString("emails");
            String jobName = runningOrCrashed.getString("jobname");

            // Check if krona.html is present.

            // Hard crash.
            // If on startup we know it completely crashed.
            if (bootUp) {
                if (!kronaExists) {
                    // Failed job.
                    updateFinishedOrCrashedDatabaseEntry(true, emails, hash, jobName);
                }
                // It still made before the crash.
                else {
                    // Still successful job.
                    updateFinishedOrCrashedDatabaseEntry(false, emails, hash, jobName);
                }
            }
            // Soft crash.
            // Check krona.html in case where a job just finished.
            if (justFinishedJob) {
                if (!kronaExists) {
                    // Failed job.
                    updateFinishedOrCrashedDatabaseEntry(true, emails, hash, jobName);
                }
                else {
                    // Still successful job.
                    updateFinishedOrCrashedDatabaseEntry(false, emails, hash, jobName);
                }
            }
        }

    }

    /**
     * Select the job which is the first in queue and is not running and sets this job to running.
     */
    void initializeExecutionForFirstInQueue(String hash) throws SQLException{
        this.statement.executeUpdate("UPDATE QUEUESUBMISSIONS SET running = 1 WHERE hash =\""+hash+"\";");
    }

    /**
     * Adds a job to the database queue
     */
    public void createNewQueueEntry(DatabaseEntry newDatabaseEntry, String jobName)throws SQLException{
        MetaMail mm = new MetaMail();
        // Login to mail.
        Session mmLoggedin = mm.metaMailLogin();
        // Send mail that your job has been submitted.
        mm.metaMailSendMail(mmLoggedin, newDatabaseEntry.getEmails(),"[MetaToKrona] Your job \""+jobName+
                "\" " +
                "has been succesfully submitted!","You will receive a link of the output in a later " +
                "email.");
        mm.metaMailRemoveOutgoingMail(mmLoggedin);
        // Insert job with variables into database.
        String statementString = "INSERT INTO QUEUESUBMISSIONS(" +
                "hash, " +
                "emails, " +
                "submission_date, " +
                "expiration_time, " +
                "expiration_date, " +
                "running, " +
                "finished_running, " +
                "jobname, " +
                "file_input, " +
                "bt2_ps, " +
                "tax_lev, " +
                "min_cu_len, " +
                "ignore_virusses, " +
                "ignore_eukaryotes, " +
                "ignore_bacteria," +
                "ignore_archaea, " +
                "stat_q, " +
                "ignore_markers, " +
                "avoid_disqm, " +
                "stat, " +
                "analysis_type, " +
                "nreads, " +
                "pres_th, " +
                "clade, " +
                "min_ab, " +
                "file_type, " +
                "min_alignment_len" +
                ") VALUES(";
        // Request each entry and insert it into the database.
        // hash
        statementString += "\""+newDatabaseEntry.getHash()+"\", ";
        // emails
        statementString += "\""+newDatabaseEntry.getEmails()+"\", ";
        // submissiondate
        statementString += newDatabaseEntry.getSubmissionDate()+", ";
        // expiration time
        statementString += newDatabaseEntry.getExpirationTime()+", ";
        // to-be-defined-date + control booleans (expiration_date, running, finished_running)
        statementString += "NULL, "+"0, "+ "0, ";
        // jobname
        statementString += "\""+newDatabaseEntry.getJobName()+"\", ";
        // filenames (unpack and make a string splitted on ",")
        String insertValue = "\"";
        for (String fileInput :newDatabaseEntry.getFileInputs()) {
            insertValue += fileInput+",";
        }
        insertValue = insertValue.substring(0, insertValue.length()-1)+"\", ";
        statementString += insertValue;
        // bowtie preset
        statementString += "\""+newDatabaseEntry.getBtPs()+"\", ";
        // taxlev
        statementString += "\""+newDatabaseEntry.getTaxLev()+"\", ";
        // min cu len
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getMinCuLen())+", ";
        // ignore virusses
        statementString += makeBooleanValue(newDatabaseEntry.getIgnoreVirusses()).toString()+", ";
        // ignore eukaryotes
        statementString += makeBooleanValue(newDatabaseEntry.getIgnoreEukaryotes()).toString()+", ";
        // ignore archaea
        statementString += makeBooleanValue(newDatabaseEntry.getIgnoreArchaea()).toString()+", ";
        // ignore bacteria
        statementString += makeBooleanValue(newDatabaseEntry.getIgnoreBacteria()).toString()+", ";
        // statq
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getStatQ())+", ";
        // marker file
        statementString += makeStringValueThatMightBeNullabe(newDatabaseEntry.getMarkerFileInput())+", ";
        // avoid disqm
        statementString += makeBooleanValue(newDatabaseEntry.getAvoidDisQM())+", ";
        // stat
        statementString += makeStringValueThatMightBeNullabe(newDatabaseEntry.getStat())+", ";
        // anaylsis type
        statementString += makeStringValueThatMightBeNullabe(newDatabaseEntry.getAnalysisType())+", ";
        // nreads
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getnReads())+", ";
        // presth
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getPresTH())+", ";
        // clade
        statementString += makeStringValueThatMightBeNullabe(newDatabaseEntry.getClade())+", ";
        // minab
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getMinAB())+", ";
        // filetype
        statementString += makeStringValueThatMightBeNullabe(newDatabaseEntry.getFileType())+", ";
        // min alignment len
        statementString += makeOtherValueThatMightBeNullabe(newDatabaseEntry.getMinAlignmentLen())+");";

        // Create entry and submit it to the database.
        this.statement.executeUpdate(statementString);

    }

    /**
     * Make from a database entry a new object that can be used to give MetaPhlAn 2 running instructions.
     */
    DatabaseEntry reCreateLastDatabaseEntry() throws SQLException{
        ArrayList <String> queriedResults;
        ResultSet rs = this.statement.executeQuery("SELECT * FROM QUEUESUBMISSIONS WHERE running = 0 AND finished_running = 0 ORDER BY submission_date asc LIMIT 1");
        // First check if result set is empty. (cant use .next() because sql only supports; TYPE_FORWARD_ONLY ResultSets).
        if (!rs.next() ) {
            System.out.println(formattedTime +"no data for state queue.");
            return null;
        }
        else {
            // Because sqlite is so beautiful, we either had to unpack the last query to begin with, or rerun the query like so.
            rs = this.statement.executeQuery("SELECT * FROM QUEUESUBMISSIONS WHERE running = 0 AND finished_running = 0 ORDER BY submission_date asc LIMIT 1");
            queriedResults = setUpResults(rs);
            // Define each variable correctly, catching nulls (java hangs on parseInt if null...)
            //hash
            String hash = queriedResults.get(0);
            // emails
            String emails = queriedResults.get(1);
            // submission date
            String submissionDate = queriedResults.get(2);
            // expirationTime
            String expirationTime = queriedResults.get(3);
            // Unnecessary values for recreating entry.
            // expirationDate (can be null, index 4)
            // running (index 5)
            // finished running (index 6)
            // jobname
            String jobName = queriedResults.get(7);
            // file input
            // grab the arraylist out of the string
            ArrayList<String> fileInputs = new ArrayList<>();
            String fileInputsRaw = queriedResults.get(8);
            String[] fileInputArray = fileInputsRaw.split(",");
            fileInputs.addAll(Arrays.asList(fileInputArray));
            // bt2 ps
            String btPs = queriedResults.get(9);
            // tax lev
            String taxLev = queriedResults.get(10);
            // min cu len (can be null)
            String minCuLen = queriedResults.get(11);
            // ig virus
            boolean ignoreVirusses = Boolean.parseBoolean(queriedResults.get(12));
            // ig euk
            boolean ignoreEukaryotes = Boolean.parseBoolean(queriedResults.get(13));
            // ig bac
            boolean ignoreBacteria = Boolean.parseBoolean(queriedResults.get(14));
            // ig arc
            boolean ignoreArchaea = Boolean.parseBoolean(queriedResults.get(15));
            // statq (can be null)
            String statQ = queriedResults.get(16);
            // ignore markers (can be null)
            String markerFile = queriedResults.get(17);
            // avoid disqm
            boolean avoidDisqm = Boolean.parseBoolean(queriedResults.get(18));
            // stat
            String stat = queriedResults.get(19);
            // analysis type
            String analysisType = queriedResults.get(20);
            // nreads (can be null)
            String nReads = queriedResults.get(21);
            // pres th (can be null)
            String presTh = queriedResults.get(22);
            // clade (can be null)
            String clade = queriedResults.get(23);
            // min ab (can be null)
            String minAB = queriedResults.get(24);
            // filetype
            String fileType = queriedResults.get(25);
            // min_alignment_len
            String minAlignmentLen = queriedResults.get(26);
            // Returns a databaseEntry object which can be used for submission to MetaPhlAn 2.
            return new DatabaseEntry(hash, fileInputs, markerFile, submissionDate, expirationTime,
                    jobName, emails, btPs, taxLev, minCuLen, minAlignmentLen, ignoreArchaea, ignoreBacteria, ignoreEukaryotes,
                    ignoreVirusses, statQ, avoidDisqm, stat, analysisType, nReads, presTh, clade, minAB, fileType);
        }
    }
    /**
     * Transfers the entries of ResultSet to an ArrayList.
     */
    private ArrayList<String> setUpResults(ResultSet acquiredRow) throws SQLException{
        ArrayList<String> entries = new ArrayList<String>();
        ResultSetMetaData rsmd = acquiredRow.getMetaData();
        // Get all column names.
        int columnsNumber = rsmd.getColumnCount();
        // Collect each column value.
        while (acquiredRow.next()){
            for (int i = 1; i <= columnsNumber; i++) {
                String columnValue = acquiredRow.getString(i);
                entries.add(columnValue);
            }
        }
        return entries;
    }
    /**
     * Turn a String value into a correct value for the sqlite database.
     */
    private String makeStringValueThatMightBeNullabe(String stringValue) {
        if (stringValue == null) {
            return "NULL";
        }
        else {
            return "\""+stringValue+"\"";
        }
    }

    /**
     * Turn a String value into an otherValue (int, double).
     */
    private String makeOtherValueThatMightBeNullabe(String otherValue) {
        if (otherValue == null) {
            return "NULL";
        }
        else {
            return otherValue;
        }
    }

    /**
     * Set boolean value true as 1 and false as 0.
     */
    private Integer makeBooleanValue(boolean booly) {
        if (booly) {
            return 1;
        }
        else {
            return 0;
        }
    }
    /**
     * Close sqlite database connection.
     */
    public void closeSQLDBConnection() throws SQLException {
        this.connection.close();
    }
}
