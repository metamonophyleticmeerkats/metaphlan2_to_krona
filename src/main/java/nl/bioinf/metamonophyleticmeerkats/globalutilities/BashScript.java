package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.io.IOException;

public class BashScript {

    private String bashFile;

    /**
     * Creates a bash script from the given commands to execute,
     * this is to bypass the errors generated with processBuilder.
     */
    public BashScript(String outputFile, String content) throws IOException {

        this.bashFile = outputFile;
        FileAndFolderInputOutput.createFile(outputFile, "#!/usr/bin/env bash\n"+content, true);
    }

    /**
     * Make the generated bash script run trough ProcessBuilder.
     */
    public ProcessBuilder MakeProcess() throws IOException {

        return new ProcessBuilder(this.bashFile);
    }

    /**
     * Remove generated bash script.
     */
    public void removeScript() {
        FileAndFolderInputOutput.deleteFile(this.bashFile);
    }
}
