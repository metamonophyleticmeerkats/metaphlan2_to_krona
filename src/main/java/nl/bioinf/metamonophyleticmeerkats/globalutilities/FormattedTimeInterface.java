package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * Makes times everywhere accessible in the program, mostly used for log and the server.
 * The format which is used is : DATETIME dd/MM/yyyy hh:mm:ss .
 */
public interface FormattedTimeInterface {
    String formattedTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MMM/yyyy hh:mm:ss a | "));
}