package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import nl.bioinf.metamonophyleticmeerkats.checkers.PropertiesChecker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class has methods to handle all file input and output.
 */
public class FileAndFolderInputOutput implements FormattedTimeInterface, WebappPaths, ConfigProperties {
    /**
     * Redirect files to their correct paths after submission of a job.
     */
    public static final ArrayList<String> fileUploader(HttpServletRequest request, String uploadPath, String attribute)
            throws IOException, ServletException{
        // Acquire upload files from the Html input.
        List<Part> fileParts = request.getParts().stream().filter(part -> attribute.equals(part.getName())).collect(
                Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">
        // Create an ArrayList in which all the filenames will be stored.
        ArrayList<String> fileInputs = new ArrayList<>();
        // Specify which folder the input must go to.
        File uploads = new File(uploadPath);
        // Create directory based on the name of the upload path (the hash).
        if (uploads.mkdir()) {
            // Add each file to the specified directory.
            for (Part filePart : fileParts) {
                // MSIE = Microsoft Internet Explorer
                String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
                // Get input from files to copy it.
                InputStream fileContent = filePart.getInputStream();
                // Put it in the directory.
                File file = new File(uploads, fileName);
                // Copy files into specified directory.
                try (InputStream input = fileContent) {
                    Files.copy(input, file.toPath());
                    // Add it to the fileInputs so it becomes clear where each file is.
                    fileInputs.add(uploadPath + fileName);
                }
            }
        }
        return fileInputs;
    }
    /**
     * Redirect marker file to the correct path and validates if it is available.
     */
    public static final String markerFileUploader(HttpServletRequest request, String uploadPath, String attribute) throws
            IOException, ServletException {
        // Acquire marker file from input.
        Part filePart = request.getPart(attribute); // Retrieves <input type="file" name="file">
        // Get the filename of the marker file.
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        // Instantiate the marker file name which might not be submitted and can be null.
        String markerFile = null;
        // If the file is not empty otherwise directly return null.
        if (!fileName.isEmpty()) {
            // Get the input from the marker file.
            InputStream fileContent = filePart.getInputStream();
            // Specify new path.
            File uploads = new File(uploadPath);
            // Make new path directory.
            if (uploads.mkdir()) {
                // Specify where marker file should be stored.
                File file = new File(uploads, fileName);
                try (InputStream input = fileContent) {
                    // Copy marker file to the directory.
                    Files.copy(input, file.toPath());
                    // Create filename location of path and filename.
                    markerFile = uploadPath + fileName;
                }
            }
        }

        return markerFile;
    }
    /**
     * Simply validates if any of the properties is null or empty, if so the program is not going to be executed.
     */
    private static final boolean checkIfFilesAndDirsIncorrectlyInitiated(){
        return webroot == null || webroot.isEmpty() ||
                metaToKronaConfigPropertiesFile == null || metaToKronaConfigPropertiesFile.isEmpty() ||
                metaToKronaMailConfigPropertiesFile == null || metaToKronaMailConfigPropertiesFile.isEmpty() ||
                metaToKronaConfigProperties == null || metaToKronaConfigProperties.isEmpty() ||
                metaToKronaMailConfigProperties == null || metaToKronaMailConfigProperties.isEmpty();
    }
    /**
     * A quick test to if the configs where correctly filled in and the output folder is made.
     */
    public static final boolean initiateWebapp() {
        // return true if everything is ok.
        createFolder(webroot+"/outputs");
        return(checkIfExists(webroot+"/outputs") && !checkIfFilesAndDirsIncorrectlyInitiated());
    }

    /**
     * Make empty file.
     */
    static final void createEmptyFile(String fileName) throws IOException {
        createFile(fileName, "", false);
    }

    /**
     *  Make folder, if already exists it is not made.
     */
    public static final void createFolder(String folderName) {
        if (checkIfExists(folderName)) {
            System.out.println(formattedTime+"Folder already exists!: "+folderName);
        }
        else {
            // Make new folder with specified name.
            boolean madeFolderSuccesfully = new File(folderName).mkdir();
            if (!madeFolderSuccesfully) {
                System.out.println(formattedTime+"Failed to make directory: "+folderName);
            }
            else {
                System.out.println(formattedTime+"Created directory: "+folderName);
            }
        }
    }
    /**
     * Read email properties for smtp, username, password and mailAddress.
     */
    static final HashMap<String, String> readMailConfigPropertiesFile (String myInputFile) {
        HashMap<String, String> myHashMap = readPropertiesFile(myInputFile);
        return PropertiesChecker.checkMetaToKronaMailConfig(myHashMap);
    }

    /**
     * Read server configuration which  returns a hashMap containing: Metaphlan2Dir, kronaDir, uploadFolder,
     * bowtie2Database, maxUploadSize, hashSize, jobHashSize, coreCount, expirationTime, dataScrubberInterval,
     * domainName, port, protocol.
     */
    static final HashMap<String, String> readConfigPropertiesFile(String myInputFile) {
        HashMap<String, String> myHashMap = readPropertiesFile(myInputFile);
        return PropertiesChecker.checkMetaToKronaConfig(myHashMap);
    }
    /**
     * Reads properties file and generates a hashMap from it.
     */
    private static final HashMap<String, String> readPropertiesFile(String myInputFile) {
        try {
            // Load file.
            File file = new File(myInputFile);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            // Closes file because its values are stored within properties.
            fileInput.close();
            // Request all keys from the properties.
            Enumeration enuKeys = properties.keys();
            // Make a hashMap in which keys and values of the properties will be stored.
            HashMap<String, String> myHashMap = new HashMap<>();
            // Store Name of the property and the property itself in the hashMap.
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);

                myHashMap.put(key, value);
            }
            return myHashMap;
        }
        catch (IOException iox) {
            // It seems the file was not there?
            return null;
        }

    }
    /**
     * Create a new file on the specified path, and write the specified contents to it.
     */
    static final void createFile(String filePath, String fileContents, boolean makeExecutable) throws IOException {
        File file = new File(filePath);
        // If file doesnt exists, then create it, if it does, delete the file and try again.
        if (!file.exists()) {
            file.createNewFile();
        }
        else {
            // Delete the old file if it exists and write the contents to it.
            System.out.println(formattedTime + "WARNING: Old file detected, did the program crash sometime before?" +
                    " Deleting old file... " + filePath);
            deleteFile(filePath);
            // Call own method again, old file should now not be present anymore.
            createFile(filePath, fileContents, makeExecutable);
        }
        // Write contents to file.
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(fileContents);
        // Finished writing, close the file.
        bw.close();

        // Make file executable.
        if (makeExecutable) {
            file.setExecutable(true, true);
        }
        System.out.println(formattedTime +"Succesfully made File!; "+filePath);
    }
    /**
     * Remove the folder at the specified path.
     */
    public static final void deleteFolder(String folderName) {
        // use method overload, make file object and give it to the method that handles deletion with file object.
        File folder = new File(folderName);
        deleteFolder(folder);
    }
    /**
     * Delete the folder at the path.
     */
    private static final void deleteFolder(File folder) {
        // All files in a file array.
        File[] files = folder.listFiles();
        if(files!=null) { //some JVMs return null for empty dirs
            // delete everything in the dir.
            // Remove recursively.
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    deleteFile(f);
                }
            }
        }
        if (!folder.delete()) {
            // Most likely no right or the folder is not there.
            System.out.println(formattedTime+"Could not delete folder: "+folder.getPath());
        }
    }
    /**
     * Removes single file.
     */
    private static final void deleteFile(File file) {
        if (!file.delete()) {
            System.out.println(formattedTime+"Could not delete file: "+file.getPath());
        }
    }
    /**
     * Removes single file with specified name.
     */
    public static final void deleteFile(String fileName) {
        File fileToBeDeleted = new File(fileName);
        deleteFile(fileToBeDeleted);
    }
    /**
     * Check if the file exists.
     */
    public static final boolean checkIfExists(String fileName) {
        if (!new File(fileName).exists()) {
            System.out.println(formattedTime+"File or Folder does not exist!: "+fileName);
            return false;
        }
        else {
            return true;
        }
    }
    /**
     * Remove all job files, not the results, those are stored in a zip.
     */
    public static final boolean cleanUpMetaToKronaFiles(String hash, String jobName) throws IOException,
            InterruptedException {
        // Make boolean for reporting if everything is removed correctly.
        boolean cleanedUpCorrectly = true;
        // Delete upload path.
        deleteFolder(metaToKronaConfigProperties.get("uploadFolder") + "/" + hash + "/");
        if (checkIfExists(metaToKronaConfigProperties.get("uploadFolder") + "/" + hash + "/")) {
            cleanedUpCorrectly = false;
        }
        // Make zip process (to zip all output).
        ProcessSerializer zipOutputSerializer = new ProcessSerializer();
        String zipExecString = "cd "+metaToKronaOutputFolder+hash+" && zip -r ./"+jobName+".zip ./*";
        BashScript zipOutputBashScript = new BashScript(webroot+"/WEB-INF/tempbashscripts/zippertemp.sh", zipExecString);
        ProcessBuilder zipOutputBashScriptProcess = zipOutputBashScript.MakeProcess();
        zipOutputSerializer.addProcess(zipOutputBashScriptProcess, "zipping");
        // Make the zip.
        zipOutputSerializer.runProcesses();
        // Cleanup bash script.
        zipOutputBashScript.removeScript();
        // Remove verbose output.
        // Metaphlan2output dir.
        deleteFolder(metaToKronaOutputFolder+hash+"/metaphlan2output/");
        if (checkIfExists(metaToKronaOutputFolder+hash+"/metaphlan2output/")) {
            cleanedUpCorrectly = false;
        }
        // Other files.
        FileAndFolderInputOutput.deleteFile(metaToKronaOutputFolder+hash+"/metaphlan2_executable.txt");
        if (checkIfExists(metaToKronaOutputFolder+hash+"/metaphlan2_executable.txt")) {
            cleanedUpCorrectly = false;
        }
        FileAndFolderInputOutput.deleteFile(metaToKronaOutputFolder+hash+"/meta2krona_output.txt");
        if (checkIfExists(metaToKronaOutputFolder+hash+"/meta2krona_output.txt")) {
            cleanedUpCorrectly = false;
        }
        FileAndFolderInputOutput.deleteFile(metaToKronaOutputFolder+hash+"/log.txt");
        if (checkIfExists(metaToKronaOutputFolder+hash+"/log.txt")) {
            cleanedUpCorrectly = false;
        }
        return cleanedUpCorrectly;
    }
}
