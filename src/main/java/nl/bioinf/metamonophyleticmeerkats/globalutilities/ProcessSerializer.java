package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.io.*;
import java.util.ArrayList;
/**
 * Processes initiated with processBuilder run parallel.
 * This is not useful for a pipeline because one process would be unable to pass date to a different process.
 */
public class ProcessSerializer implements FormattedTimeInterface {
    // Make a process- and name- "queue" in which jobs and names are stored to be handled sequentially.
    private ArrayList<ProcessBuilder> myProcesses = new ArrayList<>();
    private ArrayList<String> processNames = new ArrayList<>();
    /**
     * Add process and processName to the sequence.
     */
    public void addProcess(ProcessBuilder anotherProcess, String processName) {
        this.myProcesses.add(anotherProcess);
        this.processNames.add(processName);
    }
    /**
     * Initialize process from the available jobs.
     */
    public void runProcesses(String logOut) throws IOException, InterruptedException {
        // Create a log file of the running process.
        FileAndFolderInputOutput.createEmptyFile(logOut);

        // Validate if processes are available.
        if (this.myProcesses.size() < 1) {
            System.out.println(formattedTime +"No processes defined to run serially!");
        }
        else {
            // Run each process.
            for (int i = 0; i < this.myProcesses.size(); i++) {
                // Request process from the "queue".
                ProcessBuilder process = this.myProcesses.get(i);
                String processName = this.processNames.get(i);
                // First print which process is about to start for clarification.
                // Set stream to logfile for printing.
                PrintStream stdout = System.out;
                System.setOut(new PrintStream(new FileOutputStream(logOut, true)));
                // print out process name.
                System.out.println(processName);
                // Set stream back to normal System out.
                System.setOut(stdout);
                // Start process.
                Process runningProcess = process.start();
                // Print error log in console instead, otherwise, it is too verbose for the user.
                printErrors(runningProcess);
                boolean processIsRunning = true;
                // Test whether the process is finished, next process will only start when the current one is finished.
                while (processIsRunning) {
                    if (runningProcess.isAlive()) {
                        // Just wait until the Process is finished.
                        // Wait 2000 milliseconds to reduce the amount of requests and load on the server.
                        Thread.sleep(2000);
                    } else {
                        processIsRunning = false;
                    }
                }
            }
        }
    }
    /**
     * Initialize process from the available jobs, however, do not log this one.
     */
    void runProcesses() throws IOException, InterruptedException {
        // Run each process.
        if (this.myProcesses.size() < 1) {
            System.out.println(formattedTime +"No processes defined to run serially!");
        }
        else {
            for (ProcessBuilder process : this.myProcesses) {
                // Request process from the "queue".
                // Start process.
                Process runningProcess = process.start();
                // Print error log in console instead, otherwise, it is too verbose for the user.
                printErrors(runningProcess);
                boolean processIsRunning = true;
                // Test whether the process is finished, next process will only start when the current one is finished.
                while (processIsRunning) {
                    if (runningProcess.isAlive()) {
                        // Just wait until the process is finished.
                        // Wait 2000 milliseconds to reduce the amount of requests and load on the server.
                        Thread.sleep(2000);
                    } else {
                        processIsRunning = false;
                    }
                }
            }
        }
    }
    /**
     * Print out errors that where made during the running of a process.
     */
    private static final void printErrors(Process runningProcess) throws IOException {
        // Catch error output.
        BufferedReader in = new BufferedReader(new InputStreamReader(runningProcess.getErrorStream()));
        boolean outNull = false;
        // Test if there are no mistakes made, if not then an error will be represented.
        while (!outNull) {
            String errorLine = in.readLine();
            if (errorLine == null) {
                outNull = true;
            }
            else {
                // Print the error.
                System.out.println(formattedTime +errorLine);
            }
        }
        // End of error output.
    }
}



