package nl.bioinf.metamonophyleticmeerkats.globalutilities;
/**
 * Make the webroot and property paths accessible all throughout the package.
 */
public interface WebappPaths {
    String webroot = WebappPathsGetter.getWebappRoot();
    String metaToKronaOutputFolder = WebappPathsGetter.getWebappRoot()+"/outputs/";
    String metaToKronaConfigPropertiesFile = WebappPathsGetter.getWebappConfig();
    String metaToKronaMailConfigPropertiesFile = WebappPathsGetter.getWebappMailConfig();
}
