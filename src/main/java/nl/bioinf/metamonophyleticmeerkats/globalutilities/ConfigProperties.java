package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.util.HashMap;

/**
 * Gives every class where properties are required access to them. Sets properties to null if not specified correctly.
 */
public interface ConfigProperties extends WebappPaths {
    HashMap<String, String> metaToKronaConfigProperties = FileAndFolderInputOutput.readConfigPropertiesFile(
            metaToKronaConfigPropertiesFile);
    HashMap<String, String> metaToKronaMailConfigProperties = FileAndFolderInputOutput.readMailConfigPropertiesFile(
            metaToKronaMailConfigPropertiesFile);
}
