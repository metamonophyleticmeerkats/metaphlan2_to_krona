package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.io.File;
import java.net.URISyntaxException;
/**
 * Acquire locations of files and paths in the webapp.
 */
class WebappPathsGetter {
    /**
     * Get webapp root directory of webapp.
     */
    static final String getWebappRoot() {
        try {
            // Try to get the path.
            return WebappPaths.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()+"/../../";
        }
        catch (URISyntaxException urise) {
            // Otherwise return null.
            return null;
        }
    }
    /**
     * Get config location of webapp configuration file.
     */
    static final String getWebappConfig() {
        // If it is nowhere you get nowhere.
        if (getWebappRoot() == null) {
            return null;
        }
        else {
            // The config is in the specified path and it will look there.
            String rawConfigPath = getWebappRoot()+"/../../conf/MetaToKronaConfig.properties";
            File configFile = new File(rawConfigPath);
            // Send path back.
            if (configFile.exists()) {
                return rawConfigPath;
            }
            else {
                return null;
            }
        }
    }
    /**
     * Get location of mail config.
     */
    static final String getWebappMailConfig() {
        // If it is nowhere you get nowhere.
        if (getWebappRoot() == null) {
            return null;
        }
        else {
            // The config is in the specified path and it will look there.
            String rawConfigPath = getWebappRoot()+"/../../conf/MetaToKronaMailConfig.properties";
            File configFile = new File(rawConfigPath);
            // Send path back.
            if (configFile.exists()) {
                return rawConfigPath;
            }
            else {
                return null;
            }
        }
    }
}
