package nl.bioinf.metamonophyleticmeerkats.globalutilities;

import java.security.SecureRandom;

public class HashGenerator {
    /**
     * Generates a random hash with the a specified length which can be built up from 62 different characters.
     */
    public String generateHash(int theSize){
        // All allowed characters for generating the hash.
        String stringOfAllowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
        // Convert string to charArray for easy use in function.
        char [] characterArrayOfAllowedCharacters  = stringOfAllowedCharacters.toCharArray();

        // Instantiate secure random chooser.
        SecureRandom random = new SecureRandom();
        // Make a charArray equal to the length of which the hash is required.
        char[] hashCharacterArray = new char[theSize];

        // Choose from the array of allowed characters equal to the amount of times characters specified.
        for (int i = 0; i < hashCharacterArray.length; i++) {
            // Select secure random a value from the character array.
            int randomCharIndex = random.nextInt(stringOfAllowedCharacters.length());
            // Fill character in the array.
            hashCharacterArray[i] = characterArrayOfAllowedCharacters[randomCharIndex];

        }
        // Transform the charArray into a String.
        return new String (hashCharacterArray);
        }
    }

