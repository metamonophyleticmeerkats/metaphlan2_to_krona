CREATE TABLE QUEUESUBMISSIONS (
  hash              TEXT                  PRIMARY KEY
                                          UNIQUE
                                          NOT NULL,
  emails            TEXT                  NULL,
  submission_date   INT                   NOT NULL,
  expiration_time   INT                   NOT NULL,
  expiration_date   INT                   NULL,
  running           INT                   NULL,
  finished_running  INT                   NULL,
  jobname           TEXT                  NULL,
  file_input        TEXT                  NULL,
  bt2_ps            TEXT                  NULL,
  tax_lev           TEXT                  NULL,
  min_cu_len        INT                   NULL,
  ignore_virusses   INT                   NULL,
  ignore_eukaryotes INT                   NULL,
  ignore_bacteria   INT                   NULL,
  ignore_archaea    INT                   NULL,
  stat_q            REAL                  NULL,
  ignore_markers    TEXT                  NULL,
  avoid_disqm       INT                   NULL,
  stat              TEXT                  NULL,
  analysis_type     TEXT                  NULL,
  nreads            INT                   NULL,
  pres_th           REAL                  NULL,
  clade             TEXT                  NULL,
  min_ab            REAL                  NULL,
  file_type         TEXT                  NULL,
  min_alignment_len INT                   NULL
);


