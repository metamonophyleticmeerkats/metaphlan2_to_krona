<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MetaToKrona Results</title>
    <!-- Compiled and minified Bootstrap CSS (Bootstrap4) -->
    <link rel="stylesheet" href="css/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="css/resultPage.css"/>
    <link rel="icon" href="images/favicon.png">
</head>
<body>
    <p id="thePageResult">If you see this message, you either do not have javascript enabled, or your results don't
        exist / your job crashed.</p>
    <%--Information for user and javascript to make hrefs--%>
    <p class="error">${requestScope.faultyConfigs}</p>
    <p hidden id="hash">${requestScope.hash}</p>
    <p hidden id="job">${requestScope.job}</p>
</body>
</html>
<!-- jQuery library -->
<script src="js/jQuery/jquery-3.3.1.min.js"></script>
<!-- Popper -->
<script src="js/popper/popper.min.js"></script>
<!-- Compiled and minified Bootstrap JavaScript -->
<script src="js/bootstrap4/bootstrap.min.js"></script>
<%--own java script--%>
<%--If hash is correctly found in database and configs are not faulty, show the page--%>
<div>${requestScope.javascript}</div>