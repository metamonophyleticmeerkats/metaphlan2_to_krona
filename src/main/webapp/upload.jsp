<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MetaToKrona</title>
    <!-- Compiled and minified Bootstrap CSS (Bootstrap4) -->
    <link rel="stylesheet" href="css/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="css/uploadPage.css">
    <link rel="icon" href="images/favicon.png">
</head>

<body>
<div class="frontpage container-fluid">
    <div class="row ">
        <div id="leftdiv" class="col-sm-10 text-left">
            <img src="images/MetaphlanToKrona.png"/>
            <h1 class="Display 4">Metaphlan2 To Krona submission page</h1>
            <div class="card">
                <div class="card-header" role="tab" id="AboutHeader">
                    <h5 class="card-title">
                        <a class="collapsed" data-toggle="collapse" href="#About"
                           aria-expanded="false">
                            About
                        </a>
                    </h5>
                </div>
                <%--About collapsable div--%>
                <div id="About" class="collapse" role="tabcard">
                    <div class="card-body text-left">
                        <p>MetaToKrona is a webtool that combines Metaphlan2 and Kronatools. The reason for this is
                            because Krona plots are interactive and give an easier to understand view of a metagenomic
                            sample than the static plots Metaphlan2 produces. The webtool has a queuing system that
                            allows multiple users to submit their jobs. When a job is done running, you should receive
                            an email with a link to the output.</p>
                        <p>Description of Metaphlan2 (quoted from their
                            <a href="https://bitbucket.org/biobakery/metaphlan2">bitbucket repo</a>):</p>
                        <p class="quote">MetaPhlAn is a computational tool for profiling the composition of
                            microbial communities (Bacteria, Archaea, Eukaryotes and Viruses) from metagenomic
                            shotgun sequencing data (i.e. not 16S) with species-level. With the newly added
                            StrainPhlAn module, it is now possible to perform accurate strain-level microbial
                            profiling.</p>
                        <p>Description of Krona tools (quoted from their
                            <a href="https://github.com/marbl/Krona/wiki/KronaTools">github repo</a>):</p>
                        <p class="quote">Krona Tools is a set of scripts to create Krona charts from several
                            Bioinformatics tools as well as from text and XML files.</p>
                        <p>More information about using the webtool can be found <a
                                href="https://bitbucket.org/metamonophyleticmeerkats/metaphlan2_to_krona/overview#markdown-header-the-web-interface">here</a>.</p>
                    </div>
                </div>
            </div>
            <%--Information for the user, errors etc.--%>
            <p class="uploadState">${requestScope.uploadstate}</p>
            <p id="maxUploadSizeInBytes" hidden>${requestScope.maxUploadSizeInBytes}</p>
            <div class="errors">
                <p>${requestScope.faultyConfigs}</p>
                <p>${requestScope.faultyFiles}</p>
                <p>${requestScope.faultyJobname}</p>
                <p>${requestScope.faultyEmails}</p>
                <p>${requestScope.faultyClade}</p>
                <p>${requestScope.faultyBtps}</p>
                <p>${requestScope.faultyTaxLev}</p>
                <p>${requestScope.faultyStat}</p>
                <p>${requestScope.faultyAnalysisType}</p>
                <p>${requestScope.faultyMinCuLen}</p>
                <p>${requestScope.faultyMinAlignmentLen}</p>
                <p>${requestScope.faultyminNreads}</p>
                <p>${requestScope.faultyStatQ}</p>
                <p>${requestScope.faultyMinAB}</p>
                <p>${requestScope.faultyPresTH}</p>
            </div>
            <p id="thePageUpload"></p>
            <p id="noJavaScript"></p>
        </div>
        <div id="rightdiv" class="col-sm-2">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>Jobs in Queue</th>
                </tr>
                </thead>
                <tbody>
                <%--Print all running jobs and the jobs in the queue--%>
                <c:forEach  var="jobrun" items="${requestScope.jobsRunning}">
                    <tr class="table-success">
                        <td>${jobrun}</td>
                    </tr>
                </c:forEach>
                <c:forEach  var="job" items="${requestScope.jobsadded}">
                    <tr>
                        <td>${job}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>

<!-- jQuery library -->
<script src="js/jQuery/jquery-3.3.1.min.js"></script>
<!-- Popper -->
<script src="js/popper/popper.min.js"></script>
<!-- Compiled and minified Bootstrap JavaScript -->
<script src="js/bootstrap4/bootstrap.min.js"></script>
<%--own java script, loads in only when everything is correctly configured--%>
<div>${requestScope.javascript}</div>
</html>

