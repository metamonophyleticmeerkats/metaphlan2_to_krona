JsProps = {
    // get the file size from the hidden div so it can be checked against.
    fileSize : parseInt(document.getElementById("maxUploadSizeInBytes").innerHTML)
};

function GetFileSize() {
    // this function handles checking upload file size after any change in the file inputs
    // reset some tags first.
    document.getElementById('fileSizes').innerHTML = '';
    var fi = document.getElementById('file'); // Get the file inputs.
    var fimarker = document.getElementById('fileMarker'); // Get the marker file input.

    // Validate if any file is actually selected
    if (fi.files.length > 0) {
        // Run a loop to check each selected file by their extension, and count up the total size.
        var totalsize = 0;
        var goodextensions = true;
        var extensionformat = '';
        for (var i = 0; i <= fi.files.length - 1; i++) {
            // get the size of the file.
            var fsize = fi.files.item(i).size;
            // add to the total file size
            totalsize += fsize;
            // Show filesizes in the p-tags
            filename = fi.files.item(i).name;
            document.getElementById('fileSizes').innerHTML =
                document.getElementById('fileSizes').innerHTML + '<br /> ' +
                filename + '=' + Math.round((fsize / 1024)) + ' KB';

            // Also check if the extension is correct. If it is not, set goodextensions to false.
            fileExtension = filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
            if (extensionformat.localeCompare('') === 0) {
                if (fileExtension.localeCompare('fastq') === 0 || fileExtension.localeCompare('fq') === 0 ) {
                    extensionformat = 'fastq';

                }
                else if (fileExtension.localeCompare('fasta') === 0 || fileExtension.localeCompare('fa') === 0 ) {
                    extensionformat = 'fasta';

                }
                else if (fileExtension.localeCompare('sam') === 0) {
                    extensionformat = 'sam';

                }
                else  {
                    goodextensions = false;
                }
            }
            else {
                if (extensionformat.localeCompare('fastq') === 0) {
                    if (!(fileExtension.localeCompare('fastq') === 0 || fileExtension.localeCompare('fq') === 0 )) {
                        goodextensions = false;
                    }
                }
                else if (extensionformat.localeCompare('fasta') === 0) {
                    if (!(fileExtension.localeCompare('fasta') === 0 || fileExtension.localeCompare('fa') === 0 )) {
                        goodextensions = false;
                    }
                }
                else if (extensionformat.localeCompare('sam') === 0) {
                    if (!(fileExtension.localeCompare('sam') === 0)) {
                        goodextensions = false;
                    }
                }
            }


        }
        // also run a loop for the marker file
        for (var i = 0; i <= fimarker.files.length - 1; i++) {
            // get the size of the file
            var fsize = fimarker.files.item(i).size;
            // add it to the totalsize
            totalsize += fsize;
            // Show filesizes in the p-tags
            filename = fimarker.files.item(i).name;
            document.getElementById('fileSizes').innerHTML =
                document.getElementById('fileSizes').innerHTML + '<br /> ' +
                'MarkerFile: '+ filename + '=' + Math.round((fsize / 1024)) + ' KB';

            // Also check if the extension is correct (must be fasta).
            fileExtension = filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
            if (fileExtension.localeCompare('fasta') !== 0 && fileExtension.localeCompare('fa') !== 0) {
                goodextensions = false;
            }
        }
        // Do checks, if anything is wrong, reset the form.
        // Check if max size is not too big.
        if (totalsize > JsProps.fileSize) {
            document.getElementById('fileSizesTooBig').innerHTML = 'file(s) is/are too big! (bigger than '+(JsProps.fileSize/1024)+'KB is not accepted.)';
            document.getElementById('theForm').reset();
        }
        else {
            document.getElementById('fileSizesTooBig').innerHTML = '';
        }
        // tell user if extensions are ok.
        if (!goodextensions) {
            document.getElementById('fileExtensionWrong').innerHTML = 'file(s) has/have wrong extension(s)! Please check your input (DO NOT MIX DIFFERENT FORMATS!). only accepted format for marker file is fasta (.fa / .fasta)';
            document.getElementById('theForm').reset();
        }
        else {
            document.getElementById('fileExtensionWrong').innerHTML = '';
        }
    }
}

// Do an xmlhttprequest to get the html file containing the whole form
var xhr= new XMLHttpRequest();
xhr.open('GET', 'js/thePageUpload.html', true);
xhr.onreadystatechange= function() {
    if (this.readyState!==4) return;
    if (this.status!==200) return; // or whatever error handling you want
    document.getElementById('thePageUpload').innerHTML = this.responseText;
};
xhr.send();

// empty the no javascript warning
document.getElementById('noJavaScript').innerHTML = '';