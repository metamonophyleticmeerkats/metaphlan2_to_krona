// send an xmlhttprequest and load in the page
var xhr= new XMLHttpRequest();
xhr.open('GET', 'js/thePageResult.html', true);
xhr.onreadystatechange= function() {
    if (this.readyState!==4) return;
    if (this.status!==200) return; // or whatever error handling you want
    document.getElementById('thePageResult').innerHTML = this.responseText;
};
xhr.send();

function setLinks() {
    // after adding the page result, fill in links correctly. this function gets called whenever a mouse is hovering
    // over the page, reason for this not being outside a function is that it would get called even if the page is not
    // properly loaded yet, causing this to not work. Resulting in the user not being able to proceed past this page.
    var kronaOutput = "outputs/"+document.getElementById('hash').innerHTML+"/krona_output/krona_output.html";
    var zipDownload = "outputs/"+document.getElementById('hash').innerHTML+"/"+
        document.getElementById('job').innerHTML+".zip";
    document.getElementById("kronaResults").setAttribute("href", kronaOutput);
    document.getElementById("zipResults").setAttribute("href", zipDownload);
}