#! /usr/bin/env python3
# This script concatenates files into a single new file.
# It requires at least 2 input files and 1 output file
# The output file is the last file in the sequence, if not specified the output
#  is going to be written to the last file anyway.

import sys

def concatinator(input_files, output_file):
    """
    Concats multiple files in the order that is given on the commandline and
     write it to the last file specified on the commandline
    :param input_files: Multiple files from the commandline
    :param output_file: the last file on the commandline is where the input of
     the other files is written to
    :return:
    """
    # Open the output file and append to it
    with open(output_file, 'a') as write_to_file:
        # Iterate over input files specified on the command line
        for infile in input_files:
            # read input file
            with open(infile, 'r') as readfile:
                for line in readfile:
                    # Write line from the opened input file to the output
                    #  file without line ending
                    print(line, end="", file=write_to_file)
                    # force close read file
                readfile.close()
                # force close write file
        write_to_file.close()
    return


def main(argv=None):
    """
    Main function to run the script
    """
    if argv is None:
        argv = sys.argv
    # If less than 2 files are specified there is no reason to concatenate
    if len(argv[1:-1]) < 2:
        raise ValueError("Not enough arguments to concat multiple files.")
    else:
        concatinator(argv[1:-1], argv[-1])

    return 0

if __name__ == "__main__":
    sys.exit(main())
