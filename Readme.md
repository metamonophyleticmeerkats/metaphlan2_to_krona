![MetaToKrona logo](src/main/webapp/images/MetaphlanToKrona.png)
# **README for MetaToKrona**

# LICENSE

Copyright (c) [2018] [Casper Peters and Sylt Schuurmans] under the MIT license.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# **About**

MetaToKrona is a web-application that combines [MetaPhlAn 2](https://bitbucket.org/biobakery/metaphlan2) and [Kronatools](https://github.com/marbl/Krona/wiki/KronaTools); It converts an output from metaphlan2 to krona plot. The reason for this is because Krona plots are interactive and give an easier to understand view of a metagenomic sample than the static plots Metaphlan2 produces. The webtool has a queuing system that allows multiple users to submit their jobs. When a job is done running, you should receive an email with a link to the output. 

Description of Metaphlan2 (quoted from their bitbucket repo):

*MetaPhlAn is a computational tool for profiling the composition of microbial communities (Bacteria, Archaea, Eukaryotes and Viruses) from metagenomic shotgun sequencing data (i.e. not 16S) with species-level. With the newly added StrainPhlAn module, it is now possible to perform accurate strain-level microbial profiling.*

Description of Krona tools (quoted from their github repo):

*Krona Tools is a set of scripts to create Krona charts from several Bioinformatics tools as well as from text and XML files.*

There was also a conversion script used for this tool that was avaible on the Metaphlan1 repository, this script can be found [here](https://bitbucket.org/nsegata/metaphlan/src/2f1b17a1f4e9775fe1ce42c8481279a5e69f291f/conversion_scripts/metaphlan2krona.py?at=default&fileviewer=file-view-default).

## **MetaToKrona is made by**

Bitbucket users: @Berghopper (Casper Peters) and @tyr-lys (Sylt Schuurmans)

## **With regards to**

* @nsegata (Nicola Segata) for making metaphlan originally and making a conversion script between metaphlan output and krona input.
* [The Huttenhower Lab Department of Biostatistics, Harvard T.H. Chan School of Public Health](https://huttenhower.sph.harvard.edu/) for maintaining the current metaphlan2.
* [Battelle National Biodefense Institute (BNBI)](https://github.com/marbl/Krona/blob/master/KronaTools/LICENSE.txt) for making and maintaining KronaTools.

---

# **Table of contents**
  
* [Prerequisites before usage](#markdown-header-prerequisites-before-usage)
	* [Software](#markdown-header-software)
    
    * [Software Setup](#markdown-header-software-setup)
    
    	* [Bash](#markdown-header-bash)
        
        * [Python2, python3, perl, sqlite3 and zip tool](#markdown-header-python2-python3-perl-sqlite3-and-zip-tool)
        
        * [Java 1.8](#markdown-header-java-18)
        
        * [Tomcat 8.5](#markdown-header-tomcat-85)
        
        * [Bowtie2](#markdown-header-bowtie2)
        
        * [Metaphlan2](#markdown-header-metaphlan2)
        
        * [KronaTools](#markdown-header-kronatools)
    
    * [Installing and configuring the MetaToKrona configuration files](#markdown-header-installing-and-configuring-the-metatokrona-configuration-files)
        * [Installing both configs](#markdown-header-installing-both-configs)
        
        * [Configuring MetaToKronaConfig.properties](#markdown-header-configuring-metatokronaconfigproperties)
        
        * [Configuring MetaToKronaMailConfig.properties](#markdown-header-configuring-metatokronamailconfigproperties)
    
    * [Deploying the webapp](#markdown-header-deploying-the-webapp)

* [Admin logs and database management](#markdown-header-admin-logs-and-database-management)
    * [Admin logs and crashes](#markdown-header-admin-logs-and-crashes)
    
    * [Database management](#markdown-header-database-management)
    
    * [When stopping the server](#markdown-header-when-stopping-the-server)

* [The web interface](#markdown-header-the-web-interface)
    * [Submission page](#markdown-header-submission-page)
    
    * [Metaphlan2 options](#markdown-header-metaphlan2-options)
        * [bt2\_ps (bowtie2 preset)](#markdown-header-bt295ps-bowtie2-preset)
        
        * [tax\_lev (taxonomic level)](#markdown-header-tax95lev-taxonomic-level)
        
        * [min\_cu\_len](#markdown-header-min95cu95len)
        
        * [min\_alignment\_len](#markdown-header-min95alignment95len)
        
        * [Ignore organism types](#markdown-header-ignore-organism-types)
        
        * [stat\_q](#markdown-header-stat95q)
        
        * [ignore markers file](#markdown-header-ignore-markers-file)
        
        * [avoid\_disqm](#markdown-header-avoid95disqm)
        
        * [stat](#markdown-header-stat)
        
        * [analysis type](#markdown-header-analysis-type)
        
        * [nreads](#markdown-header-nreads)
        
        * [pres\_th](#markdown-header-pres95th)
        
        * [clade](#markdown-header-clade)
        
        * [min\_ab](#markdown-header-min95ab)
    
    * [Output](#markdown-header-output)
        * [E-mails](#markdown-header-e-mails)
        
        * [Output page](#markdown-header-output-page)
        
        * [Zip contents](#markdown-header-zip-contents)
    
    * [Queuing system](#markdown-header-queuing-system)
        
---

# **Prerequisites before usage**

**DISCLAIMER: THIS SOFTWARE ONLY RUNS ON LINUX**

All software that is installed, needs to be accesible (have correct file permissions) by the user that runs tomcat as well.

## **Software**
* [bash](https://www.gnu.org/software/bash/) language (should be pretty standard under linux).
* [python2 and python3](https://www.python.org/downloads/) BOTH with [numpy](http://www.numpy.org/) and [biopython](http://biopython.org/) installed.
* [perl 5](https://www.perl.org/).
* [SQLite](https://sqlite.org/) version 3 or higher (optional, might be handy for managing the database while tomcat server is turned off).
* [zip](https://linux.die.net/man/1/zip) tool (command line linux).
* [Java version 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Tomcat 8.5.X](https://tomcat.apache.org/download-80.cgi) server with [sqlite drivers](https://bitbucket.org/xerial/sqlite-jdbc/downloads/) installed and correct configuration files.
* A working [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) install with Bowtie2 commands in the system PATH variable.
* A working Metaphlan2 install ([Metaphlan2 repository](https://bitbucket.org/biobakery/metaphlan2)) ([latest known working commit](https://bitbucket.org/biobakery/metaphlan2/commits/16d339affe6c3c3bdf32c1464dff32840d1c7a74)).
* [KronaTools](https://github.com/marbl/Krona/wiki/KronaTools) (latest known working: **version 2.7**).


## **Software Setup**

### **Bash**

This should be already installed under linux.

### **Python2, python3, perl, sqlite3 and zip tool**

Python2, python3, perl and zip can be downloaded with your linux package manager depending on which distribution is used. The python2 and python3 packages numpy and biopython can be installed using [pip2 and pip3](https://pypi.python.org/pypi/pip) respectively.

### **Java 1.8**

Installation instruction for Java 8 can be found [here](https://docs.oracle.com/javase/8/docs/technotes/guides/install/linux_jdk.html).

### **Tomcat 8.5**

Just download Tomcat 8.5.x from [here](https://tomcat.apache.org/download-80.cgi) and unpack it in any directory. From now on in the readme, the folder containing all Tomcat files will be refered to as: "(TOMCATROOT)". All executable files within Tomcat need to have correct executable permissions.

After having Tomcat installed, you also need sqlite drivers for the webapp. The sqlite drivers can be downloaded [here](https://bitbucket.org/xerial/sqlite-jdbc/downloads/). After downloading the sqlite driver, the jar can be placed under the "(TOMCATROOT)/lib/" directory. However, to make Tomcat recognize the driver, additional configuration is required. You need to add a few things to two config files; "(TOMCATROOT)/webapps/manager/meta-inf/context.xml" and "(TOMCATROOT)/webapps/manager/web-inf/web.xml".

* In the config.xml add the following somewhere in between the Context tags:
```
<Resource name="jdbc/sqlite"
     type="javax.sql.DataSource"
     driverClassName="org.sqlite.JDBC"
     url="jdbc:sqlite:(TOMCATROOT)/webapps/ROOT/WEB-INF/database/MetaToKrona.db"
    />
```
**(TOMCATROOT) needs to be replaced with your own tomcat directory.**

* In the web.xml add the following somewhere in between the web-app tags
```
<resource-ref>
     <res-ref-name>jdbc/sqlite</res-ref-name>
     <res-type>javax.sql.DataSource</res-type>
</resource-ref>
```

The Sqlite drivers should be installed now.

Last but not least, the configs of this webapp need to be installed under "(TOMCATROOT)/conf". Further details of how to install and configure these files can be found under the chapter [*Installing and configuring the MetaToKrona configuration files*](#--installing-and-configuring-the-metatokrona-configuration-files--).

### **Bowtie2**

Bowtie2 can be installed via linux package manager or it can be download [here](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#obtaining-bowtie-2) and installed from their source. For Metaphlan2 to function correctly, all Bowtie2 commands must be in the system PATH variable. All executable files need to have correct executable permissions.

### **Metaphlan2**

  [latest known working commit that is compatible with MetaToKrona](https://bitbucket.org/biobakery/metaphlan2/commits/16d339affe6c3c3bdf32c1464dff32840d1c7a74) 
  
  This is version 2.7.1, however version numbers don't seem to get updated very well, thus you should just download the last known working commit. This can be found [here](https://bitbucket.org/biobakery/metaphlan2/commits/16d339affe6c3c3bdf32c1464dff32840d1c7a74). mercurial is used for metaphlan2.
  Mercurial information about commit:
  
```
$ hg sum
parent: 309:16d339affe6c tip
 allowing stderr messages to be printed when testing external software
branch: default
commit: 13 modified, 2 subrepos
update: (current)
```

Installing metaphlan2 itself is documented pretty well on its [repository](https://bitbucket.org/biobakery/metaphlan2) and [wiki](https://bitbucket.org/biobakery/biobakery/wiki/metaphlan2). All executable files need to have correct executable permissions.

### **KronaTools**

KronaTools can be downloaded [here](https://github.com/marbl/Krona/wiki/KronaTools). After downloading, just extract the files anywhere on your system.

## **Installing and configuring the MetaToKrona configuration files**

If one or both config files are somehow incorrectly configured, the webapp will prevent you from using it. **So read this section carefully.**

### **Installing both configs**

Before deploying, MetaToKrona needs to have its configuration files in the correct tomcat directory to run correctly. Example configuration files can be found on this repository under [src/main/configs/](src/main/configs/). There will be two configuration files; *MetaToKronaConfig.properties* and  *MetaToKronaMailConfig.properties*. Both of these properties file need to be copied / made under the "(TOMCATROOT)/conf/" directory.

### **Configuring MetaToKronaConfig.properties**

```python
# Configuration for MetaToKrona web server interface.

# metaphlan2 root directory
metaphlanTwoDir=/home/ccpeters/Desktop/Software/metaphlan2/
# krona tools ImportText.pl location
kronaImportTextScript=/home/ccpeters/Desktop/Software/KronaTools-2.7/scripts/ImportText.pl
# upload folder for submitting files. (DO NOT DEFINE THIS AS THE OUTPUT DIRECTORY THAT IS USED UNDER WEBAPPS)
uploadFolder=/home/ccpeters/uploadtests/
# bowtie2  database file of the MetaPhlAn database. (use full directory, standard = DEFAULT which means standard
# metaphlan2 database will be used.)
# Used if --input_type is fastq, fasta, multifasta, or multifastq
# WARNING: THIS IS EXPIRIMENTAL AND NOT TESTED, FALL BACK TO DEFAULT IF THIS CAUSES ISSUES.
bowtieDatabase=DEFAULT
# max allowed upload size for any form submission of combined file sizes. If combined filesize is bigger than this, form
# for the user will be reset. (min:1024, max:Infinite)
maxUploadSizeInBytes=21474836480
# hash size for generating hashes that will be used to access the output later on (min:16, max:255)
# generally it is recommended to keep this as high as possible, as this partially acts as a security feature to prevent
# people seeing each others outputs. 62 random characters can be used to generate the hash, so to calculate the amount
# of unique hashes one can use the following formula; 62^(hashSize)
hashSize=255
# hash size for the jobname if jobname is not specified by user, the jobname is used to make a zip output so it can be
# referenced by the user when downloaded. Because this is not a security feature unlike the previous hash, it is
# recommended to keep this hash fairly low (less filename clutter when downloaded). (min:1, max:255)
# 62 random characters can be used to generate the hash, so to calculate the amount of unique hashes one can use the
# following formula; 62^(jobHashSize)
jobHashSize=10
# Cores / threads that metaphlan2 is allowed to use when calculating output (min:1, max:Infinite)
coreCount=10
# expiration time of a finished output (after job has finished). The output of a job will be deleted after this time.
# (min:300, max:Infinite)
expirationTimeInSeconds=300
# database scrubber interval in milliseconds. The database scrubber does several checks (removing old entries,
# starting jobs and reporting finished/crashed jobs). This interval prevents the database being queried too often.
# (min:1000, max:Infinite)
databaseScrubberIntervalMillis=5000
# the domain name of the server, this can be an ip-adress, hostname or domain name.
domainName=192.168.59.108
# the port your web server is running on
port=8080
# used protocol of the server (important for checking when to stop the database scrubber).
protocol=http
```

* **metaphlanTwoDir** needs to be the directory pointing to where metaphlan2 is installed.
* **kronaImportTextScript** is the script used for making the Krona plot, it should be located under your KronaTools is installed: "KronaTool-2.7/scripts/ImportText.pl"
* **uploadFolder** is the folder where all input files will be stored, these files should be automatically removed once a job is finished.
* **bowtieDatabase** is the BowTie2 database file of the MetaPhlAn database. THIS FEATURE IS EXPIRIMENTAL AND IS RECOMMENDED TO BE LEFT ON "DEFAULT".
* **maxUploadSizeInBytes** is the maximum allowed combined filesize of a single job submission, if a user tries specify files that combined are larger than this threshold, the form will be reset and the user will be prompted that his or her files are too big. This threshold is set in bytes and can be as big as you want.
* **hashSize** is the length of the hash under which a user job will be saved, this is a security feature and this is why **Tomcat should not allow browsing of any directories**. For each job a random hash of this size will be generated and saved under "(TOMCATROOT)/webapps/ROOT/outputs/". It is recommended to leave this hashSize at 255, this makes it harder to try bruteforce guess the output folder.
* **jobHashSize** is the length of the hash used for the jobname, this is partially a security feature to make it obscure for users to know each others output, only the person that owns this job will know their jobname as they received an email of this.
* **coreCount** is the amount of logical cpu cores Bowtie2 is allowed to use while analyzing a sample.
* **expirationTimeInSeconds** is the amount of time in seconds a users output will be kept after a job is finished. Thus it is recommended for users to download their output before this time.
* **databaseScrubberIntervalMillis** is the amount of time in between database scans. This is used for preventing the database being queried too much. The database holds all jobs and the database scanner will handle starting jobs, deleting outputs and reporting crashed jobs etc. This is recommended to be kept fairly high (every half hour or so) because the scanner will still keep going if there's a lot of jobs in the queue. The interval is specified in milliseconds.
* **domainName** is the domain name that the server will be accessible from to the outside world, this can be an IP address, hostname or domain name. DO NOT PUT THE PROTOCOL (http://) OR PORT (:8080) IN THIS VARIABLE.
* **port** is the port the webserver is running on (default: 8080)
* **protocol** the protocol used for accessing the webserver (http / https)

A combination of the protocol, domainname and port will be used for mailing users the correct links. 

**Note: the database scrubber will shutdown when protocol:localhost:port is not accessible, this is to make the "catalina stop" command function properly. Make sure that localhost is accesible. and that the port and protocol are correctly defined.**

### **Configuring MetaToKronaMailConfig.properties**

This configuration file is used for defining which email address and protocol gets used to send the outputs of the program.

```python
# Configuration for MetaToKrona web server interface. (mail)
# The following configuration file is for configuring the email address with which users will be notified of their ran
# jobs. The protocol used is smtp and is not super-secure. However, all outgoing mail is deleted right after being sent,
# so it does not leave any traces for other users. Please use an email address that does contain sensitive information,
# and preferably, make a separate account just for this webapp (gmail is preferred). The account needs to have access
# for less secure apps on.

# smtp server to use
smtpHost=smtp.gmail.com
# username of email
username=metatokrona
# password of email
password=mypassword
# full mail address
internetAddress=metatokrona@gmail.com
```

* *smptHost* needs to be a valid smtp Host server, if not correctly configured, mail might no be sent.
* *username* needs to be the username of the account.
* *password* needs to be the password of the account.
* *internetAdress* needs to be the full email address of your gmail account

**The email used needs to a gmail account with less secure apps turned ON. If this is not the case, some things might not work 100% correctly. This is why it's also recommended to leave the smptHost  option as it is. It is also recommended to use a seperate account that does not hold important info, as smtp is used and the password is stored in plain text.**

## **Deploying the webapp**

Last but not least, we need to deploy the webapp once everything is correctly configured. There's two ways of going about this; 

Downloading the war from our repository: [MetaToKrona-1.0.war](MetaToKrona-1.0.war) rename it to "ROOT.war" and then copy it under the "(TOMCATROOT)/webapps/" directory. After placing it in the webapp directory, delete the standard ROOT directory that is already present. After which the `catalina run` can be used to start tomcat and tomcat should automatically explode and deploy the webapp now. After this, everything should be ok.

The second option is to clone the repository and build the war with gradle. with the `gradle war` command. After which you can deploy the war just like in the steps above.

After the webapp is deployed you can start the server with either `catalina run`/`catalina start` and should preferably be stopped with the `catalina stop` command. If a job is still running or the database scanning interval is big, stopping the server might take a while.

# **Admin logs and database management**

## **Admin logs and crashes**

All output of our webapp is redirected to an admin log which might be useful to reference if you are having issues. The log file can be found under the "(TOMCATROOT)/webapp/ROOT/WEB-INF/MetaToKrona.log".
In the event of a server crash, no jobs or data should be lost, the only thing that will happen is that a **currently running** job might not have finished correctly and needs to be re-submitted by the user, they will receive an e-mail stating the latter too.

## **Database management**

If neccesary, the database can be accessed in the "(TOMCATROOT)/webapps/ROOT/WEB-INF/database/MetaToKrona.db" file. It can be accessed with sqlite3, however before doing this, make sure tomcat is turned off.

## **When stopping the server**

In the case of stopping the server with `./catalina.sh stop`, you may expirience the following error:

```
01-Feb-2018 16:25:05.318 WARNING [localhost-startStop-2] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesJdbc The web applic
ation [ROOT] registered the JDBC driver [org.sqlite.JDBC] but failed to unregister it when the web application was stopped. To prevent a mem
ory leak, the JDBC Driver has been forcibly unregistered.
01-Feb-2018 16:25:05.319 WARNING [localhost-startStop-2] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web app
lication [ROOT] appears to have started a thread named [pool-1-thread-1] but has failed to stop it.This is very likely to create a memory le
ak. Stack trace of thread:
 java.lang.Thread.sleep(Native Method)
 nl.bioinf.metamonophyleticmeerkats.database.DatabaseScrubberBackgroundProcess.databaseScrubLoop(DatabaseScrubberBackgroundProcess.java:75)
 nl.bioinf.metamonophyleticmeerkats.database.DatabaseScrubberBackgroundProcess.run(DatabaseScrubberBackgroundProcess.java:43)
 java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
 java.util.concurrent.FutureTask.run(FutureTask.java:266)
 java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
 java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
 java.lang.Thread.run(Thread.java:748)
```

This happens as of the current version because the sqlite drivers are not properly closed, this is however, nothing to be worried about and can be ignored.

# **The web interface**


Here the web interface will be explained in detail and what options are available for using with metaphlan2.

## **Submission page**

An example of the main submission page:

![submission page](ReadmeImages/SubmissionPage.png)

The "About" and "Metaphlan options" can be clicked to reveal the About information and extra Metaphlan2 options respectively:

![about](ReadmeImages/About.png)

![metaoptions](ReadmeImages/MetaOptions.png)

The minimal neccesities for running a job is 1 or more email addresses and a fastq, fasta or sam file that can be analyzed. A jobname can also be specified, but is however not neccesary and may be more desirable if you want your job to stay anonymous. If a jobname is not specified, a random one will be created.

## **Metaphlan2 options**

Not all metaphlan2 options are supported for our webtool, but the ones that are implemented will be further explained below.

### **bt2\_ps (bowtie2 preset)**

![bt2_ps](ReadmeImages/MetaOptions/bt2_ps.png)

The bowtie2 preset can be 1 of four options; 

* sensitive
* very-sensitive
* sensitive-local
* very-sensitive-local

this option will be applied only when a multifasta file is provided. "sensitive" and "very-sensitive" are for end-to-end alignment and "sensitive-local" and "very-sensitive-local" are for local alignment.

### **tax\_lev (taxonomic level)**

![tax_lev](ReadmeImages/MetaOptions/tax_lev.png)

tax\_lev or taxonomic level, is the taxonomic level for the relative abundance output:

* 'a' : all taxonomic levels
* 'k' : kingdoms (Bacteria and Archaea) only
* 'p' : phyla only
* 'c' : classes only
* 'o' : orders only
* 'f' : families only
* 'g' : genera only
* 's' : species only

### **min\_cu\_len**

![min_cu_len](ReadmeImages/MetaOptions/min_cu_len.png)

min\_cu\_len is the minimum total nucleotide length for the markers in a clade for estimating the abundance without considering sub-clade abundances. The default is set at 2000.

### **min\_alignment\_len**

![min_alignment_len](ReadmeImages/MetaOptions/min_alignment_len.png)

The sam records for aligned reads with the longest subalignment length smaller than this threshold will be discarded. The default is "None" in end-to-end alignment, with local alignment, this value will be automatically set to 100.

### **Ignore organism types**

![ignores](ReadmeImages/MetaOptions/ignores.png)

This selects which organisms / domains of life will be ignored while scanning.

### **stat\_q**

![stat_q](ReadmeImages/MetaOptions/stat_q.png)

The quantile value for the robust average. Default is set at 0.1.

### **ignore markers file**

![ignore_markers](ReadmeImages/MetaOptions/ignore_markers.png)

File containing a list of markers to ignore in fasta format. 

### **avoid\_disqm**

![avoid_disqm](ReadmeImages/MetaOptions/avoid_disqm.png)

Deactivate the procedure of disambiguating the quasi-markers based on the marker abundance pattern found in the sample. It is generally recommended to keep the disambiguation procedure in order to minimize false positives.

### **stat**


![statistical_analysis](ReadmeImages/MetaOptions/statistical_analysis.png)

(expirimental feature) Statistical approach for converting marker abundances into clade abundances. The following options can be selected:

* 'avg\_g'  : clade global (i.e. normalizing all markers together) average
* 'avg\_l'  : average of length-normalized marker counts
* 'tavg\_g' : truncated clade global average at stat\_q quantile
* 'tavg\_l' : trunated average of length-normalized marker counts (at stat\_q)
* 'wavg\_g' : winsorized clade global average (at stat\_q)
* 'wavg\_l' : winsorized average of length-normalized marker counts (at stat\_q)
* 'med'    : median of length-normalized marker counts

Default is set at 'tavg\_g'

### **analysis type**

![analysis](ReadmeImages/MetaOptions/analysis.png)

Type of analysis to perform: 

* rel\_ab: profiling a metagenomes in terms of relative abundances
* rel\_ab\_w_read_stats: profiling a metagenomes in terms of relative abundances and estimate the number of reads coming from each clade.
* reads\_map: mapping from reads to clades (only reads hitting a marker)
* clade\_profiles: normalized marker counts for clades with at least a non-null marker
* marker\_ab\_table: normalized marker counts (only when > 0.0 and normalized by metagenome size if nreads is specified)
* marker\_pres\_table: list of markers present in the sample (threshold at 1.0 if not differently specified with pres\_th)

Default is set at 'rel\_ab'

### **nreads**

![nreads](ReadmeImages/MetaOptions/nreads.png)

The total number of reads in the original metagenome. It is used only when anaylsis type marker\_table is specified for normalizing the length-normalized counts with the metagenome size as well. No normalization applied if nreads is not specified.

### **pres\_th**

![pres_th](ReadmeImages/MetaOptions/pres_th.png)

Threshold for calling a marker present with the analysis type marker\_pres\_table option.

### **clade**

![clade](ReadmeImages/MetaOptions/clade.png)

The clade for clade\_specific\_strain\_tracker analysis. For example: "s__Bacteroides_caccae"

### **min\_ab**

![min_ab](ReadmeImages/MetaOptions/min_ab.png)

The minimum percentage abundace for the clade in the clade\_specific\_strain\_tracker analysis. (1.0 = 1%)

## **Output**

### **E-mails**

The way the web-interface gives you your output and keeps track of how your job is doing is via e-mails. There are 3 possible emails that you will receive.

* When a job is submitted succesfully:

![submitted](ReadmeImages/SubmittedMessage.png)

* When a job has finished correctly:

![finished](ReadmeImages/OutputMessage.png)

* Or when a job has crashed:

![crashed](ReadmeImages/CrashedMessage.png)

In the case of when a job has crashed, you can still download the failed output and see what went wrong.

### **Output page**

When a job has finished correctly, the link in your email should lead to the following output page:

![result](ReadmeImages/ResultPage.png)

Here you can choose to either download your output (which is recommended to, because your files will probably not be kept forever!) or view the krona plot directly. When downloading the zip you will be prompted like so:

![zip](ReadmeImages/ZipDownload.png)

The following plot is an example generated with the example fastq file located under [tutorial/exampleInput/](tutorial/exampleInput/).

![KronaOutput](ReadmeImages/KronaOutput.png)

### **Zip contents**

The zip contains multiple files; the krona plot output, metaphlan2 output, converted metaphlan2 output for krona, a log containing which step was last executed (useful to know if a program crashed) and a metaphlan2 executable that was used with the specific parameters that you gave it (also useful in case it crashed, so you can submit a job again with the same or slightly edited parameters). An example zip file can also be found under [tutorial/exampleOutput/](tutorial/exampleOutput/).

## Queuing system

Your submission will not be executed right away, instead, it is placed in a queue viewable on the right side of the webpage. The order in which jobs will be executed is from top first and bottom last. Once it is your job's turn to be ran, it will light up green on a page refresh. An example queue can be seen here:

![queue](ReadmeImages/JobQueue.png)